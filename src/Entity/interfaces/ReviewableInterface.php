<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/16/20
 * Time: 20:17
 */

namespace App\Entity\interfaces;


interface ReviewableInterface
{
    public function getReviews();

    public function addReview($review);

    public function removeReview($review);

    public function getAverageRating();

    public function setAverageRating(?float $averageRating);
}

