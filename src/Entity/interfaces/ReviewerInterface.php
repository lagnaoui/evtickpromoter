<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/16/20
 * Time: 20:24
 */

namespace App\Entity\interfaces;

interface ReviewerInterface
{
    public function getFullName();

    public function setFullName(string $fullName);

    public function getPhone();

    public function setPhone(string $phone);

}
