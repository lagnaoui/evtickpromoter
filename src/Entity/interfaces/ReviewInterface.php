<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/16/20
 * Time: 20:22
 */

namespace App\Entity\interfaces;
use Symfony\Component\Security\Core\User\UserInterface;

interface ReviewInterface
{

    public function getRating();

    public function setRating(int $rating);

    public function getComment();

    public function setComment(?string $comment);

    public function getReviewer();

    public function setReviewer(UserInterface $author);

}
