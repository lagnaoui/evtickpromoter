<?php

namespace App\Entity;

use App\Repository\BeneficiaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=BeneficiaryRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class Beneficiary
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $fullname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Torder::class, mappedBy="beneficiary")
     */
    private $torder;

    public function __construct()
    {
        $this->torder = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Torder[]
     */
    public function getTorder(): Collection
    {
        return $this->torder;
    }

    public function addTorder(Torder $torder): self
    {
        if (!$this->torder->contains($torder)) {
            $this->torder[] = $torder;
            $torder->setBeneficiary($this);
        }

        return $this;
    }

    public function removeTorder(Torder $torder): self
    {
        if ($this->torder->removeElement($torder)) {
            // set the owning side to null (unless already changed)
            if ($torder->getBeneficiary() === $this) {
                $torder->setBeneficiary(null);
            }
        }

        return $this;
    }
}
