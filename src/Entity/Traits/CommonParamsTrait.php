<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/13/20
 * Time: 23:20
 */

namespace App\Entity\Traits;

use JMS\Serializer\Annotation as Serializer;

trait CommonParamsTrait
{
    /**
     *
     * @ORM\Column(name="platform", type="string", columnDefinition="enum('ios', 'android', 'web')")
     */
    private $os;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $version;

    public function getOs(): ?string
    {
        return $this->os;
    }

    public function setOs(string $os): self
    {
        $this->os = $os;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }
}

