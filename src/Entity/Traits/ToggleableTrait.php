<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/1/20
 * Time: 18:33
 */

namespace App\Entity\Traits;


use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

trait ToggleableTrait
{
    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose
     * @Groups({"signup"})
     */
    protected $enabled = false;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = (bool)$enabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }
}