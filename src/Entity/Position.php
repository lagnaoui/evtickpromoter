<?php

namespace App\Entity;

use App\Repository\PositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PositionRepository::class)
 * @Serializer\ExclusionPolicy("ALL")
 */
class Position
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $country_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country_name_en;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country_name_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country_name_fr;

    /**
     * @ORM\Column(type="integer")
     */
    private $city_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city_name_en;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city_name_ar;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city_name_fr;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=6)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=6)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string")
     */
    private $postal_code;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="position")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="position")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=EventDate::class, mappedBy="position")
     */
    private $eventDates;

    /**
     * @ORM\OneToMany(targetEntity=Admin::class, mappedBy="position")
     */
    private $admins;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->eventDates = new ArrayCollection();
        $this->admins = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getCityNameFr();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountryCode(): ?string
    {
        return $this->country_code;
    }

    public function setCountryCode(string $country_code): self
    {
        $this->country_code = $country_code;

        return $this;
    }

    public function getCountryNameEn(): ?string
    {
        return $this->country_name_en;
    }

    public function setCountryNameEn(string $country_name_en): self
    {
        $this->country_name_en = $country_name_en;

        return $this;
    }

    public function getCountryNameAr(): ?string
    {
        return $this->country_name_ar;
    }

    public function setCountryNameAr(string $country_name_ar): self
    {
        $this->country_name_ar = $country_name_ar;

        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->city_id;
    }

    public function setCityId(int $city_id): self
    {
        $this->city_id = $city_id;

        return $this;
    }

    public function getCityNameEn(): ?string
    {
        return $this->city_name_en;
    }

    public function setCityNameEn(string $city_name_en): self
    {
        $this->city_name_en = $city_name_en;

        return $this;
    }

    public function getCityNameAr(): ?string
    {
        return $this->city_name_ar;
    }

    public function setCityNameAr(string $city_name_ar): self
    {
        $this->city_name_ar = $city_name_ar;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryNameFr()
    {
        return $this->country_name_fr;
    }

    /**
     * @param mixed $country_name_fr
     */
    public function setCountryNameFr($country_name_fr): void
    {
        $this->country_name_fr = $country_name_fr;
    }

    /**
     * @return mixed
     */
    public function getCityNameFr()
    {
        return $this->city_name_fr;
    }

    /**
     * @param mixed $city_name_fr
     */
    public function setCityNameFr($city_name_fr): void
    {
        $this->city_name_fr = $city_name_fr;
    }


    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postal_code;
    }

    public function setPostalCode(string $postal_code): self
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setPosition($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getPosition() === $this) {
                $user->setPosition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setPosition($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getPosition() === $this) {
                $event->setPosition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EventDate[]
     */
    public function getEventDates(): Collection
    {
        return $this->eventDates;
    }

    public function addEventDate(EventDate $eventDate): self
    {
        if (!$this->eventDates->contains($eventDate)) {
            $this->eventDates[] = $eventDate;
            $eventDate->setPosition($this);
        }

        return $this;
    }

    public function removeEventDate(EventDate $eventDate): self
    {
        if ($this->eventDates->removeElement($eventDate)) {
            // set the owning side to null (unless already changed)
            if ($eventDate->getPosition() === $this) {
                $eventDate->setPosition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Admin[]
     */
    public function getAdmins(): Collection
    {
        return $this->admins;
    }

    public function addAdmin(Admin $admin): self
    {
        if (!$this->admins->contains($admin)) {
            $this->admins[] = $admin;
            $admin->setPosition($this);
        }

        return $this;
    }

    public function removeAdmin(Admin $admin): self
    {
        if ($this->admins->removeElement($admin)) {
            // set the owning side to null (unless already changed)
            if ($admin->getPosition() === $this) {
                $admin->setPosition(null);
            }
        }

        return $this;
    }
}
