<?php

namespace App\Entity;

use App\Entity\interfaces\ReviewableInterface;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Traits\ToggleableTrait;
use App\Entity\Traits\TimestampableTrait;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class User implements UserInterface, \Serializable
{
    public const CATERER = "caterer";
    public const CUSTOMER = "customer";
    public const ADMIN = "admin";
    use ToggleableTrait, TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "user_info", "feed_images"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "CatererReviews", "user_info", "GenerateInvoice", "feed_images", "new_order"})
     */
    protected $fullName;

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("city")
     * @Groups({"showProfile", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "user_info", "feed_images"})
     * @return string
     */
    public function getCity()
    {
        if(!empty($this->getPosition())) {
            $locale = $GLOBALS['request'] ? $GLOBALS['request']->getLocale() : "ar";
            switch ($locale) {
                case "en":
                    return $this->getPosition()->getCityNameEn();
                    break;
                case "ar":
                    return $this->getPosition()->getCityNameAr();
                    break;
                default:
                    return $this->getPosition()->getCityNameFr();
                    break;
            }
        }
        return null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("country")
     * @Groups({"showProfile", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "user_info", "feed_images"})
     * @return string
     */
    public function getCountry()
    {
        if(!empty($this->getPosition())) {
            $locale = $GLOBALS['request'] ? $GLOBALS['request']->getLocale() : "ar";
            switch ($locale) {
                case "en":
                    return $this->getPosition()->getCountryNameEn();
                    break;
                case "ar":
                    return $this->getPosition()->getCountryNameAr();
                    break;
                default:
                    return $this->getPosition()->getCountryNameFr();
                    break;
            }
        }
        return null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("country_code")
     * @Groups({"showProfile", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "user_info"})
     * @return string
     */
    public function getCountryCode()
    {
        return $this->getPosition() ? $this->getPosition()->getCountryCode() : null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("city_id")
     * @Groups({"showProfile", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "user_info"})
     * @return string
     */
    public function getCityId()
    {
        return $this->getPosition() ? $this->getPosition()->getCityId() : null;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->getPosition() ? $this->getPosition()->getPostalCode() : null;
    }

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Serializer\Expose
     * @Groups({"showProfile", "user_info", "new_order"})
     */
    protected $phone;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Serializer\Expose
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "user_info", "new_order"})
     */
    protected $email;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "user_info"})
     */
    protected $latitude;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "user_info"})
     */
    protected $longitude;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $roles;

    /**
     * @ORM\OneToOne(targetEntity=Session::class, mappedBy="user", cascade={"persist", "remove"})
     * @Serializer\Expose
     * @Groups({"Auth"})
     */
    protected $session;

    /**
     * @ORM\OneToOne(targetEntity=AvatarUser::class, mappedBy="owner", cascade={"persist", "remove"})
     * @Serializer\Exclude
     */
    protected $avatar;

    /**
     * @ORM\OneToOne(targetEntity=Device::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $device;


    /**
     * @ORM\ManyToOne(targetEntity=Position::class, inversedBy="users")
     * @Serializer\Exclude
     */
    private $position;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $locale;

    /**
     * @ORM\OneToMany(targetEntity=Torder::class, mappedBy="user")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity=TicketRating::class, mappedBy="reviewer", orphanRemoval=true)
     */
    private $ticketRatings;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->ticketRatings = new ArrayCollection();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("avatar_url")
     * @Groups({"showProfile", "myRequest", "Caterer_showMyReceivedRequest", "customer_showMyReviews", "customerCateresFeed", "CatererReviews", "user_info", "feed_images"})
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->getAvatar() ? $_ENV["AWS_BUCKET_URL"] . $this->getAvatar()->getPath() : null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }
    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(Session $session): self
    {
        $this->session = $session;

        // set the owning side of the relation if necessary
        if ($session->getUser() !== $this) {
            $session->setUser($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param $avatar
     * @return User
     */
    public function setAvatar($avatar): self
    {
        $this->avatar = $avatar;

        // set the owning side of the relation if necessary
        if ($avatar->getOwner() !== $this) {
            $avatar->setOwner($this);
        }

        return $this;
    }

    public function serialize()
    {
        return serialize($this->id);
    }

    public function unserialize($serialized)
    {
        $this->id = unserialize($serialized);
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(Device $device): self
    {
        $this->device = $device;

        // set the owning side of the relation if necessary
        if ($device->getUser() !== $this) {
            $device->setUser($this);
        }

        return $this;
    }


    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return Collection|Torder[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Torder $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUser($this);
        }

        return $this;
    }

    public function removeOrder(Torder $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getUser() === $this) {
                $order->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TicketRating[]
     */
    public function getTicketRatings(): Collection
    {
        return $this->ticketRatings;
    }

    public function addTicketRating(TicketRating $ticketRating): self
    {
        if (!$this->ticketRatings->contains($ticketRating)) {
            $this->ticketRatings[] = $ticketRating;
            $ticketRating->setReviewer($this);
        }

        return $this;
    }

    public function removeTicketRating(TicketRating $ticketRating): self
    {
        if ($this->ticketRatings->removeElement($ticketRating)) {
            // set the owning side to null (unless already changed)
            if ($ticketRating->getReviewer() === $this) {
                $ticketRating->setReviewer(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getFullName();
    }

}
