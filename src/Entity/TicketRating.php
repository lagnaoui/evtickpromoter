<?php

namespace App\Entity;

use App\Repository\TicketRatingRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\interfaces\ReviewerInterface;
use App\Entity\interfaces\ReviewInterface;
use App\Entity\Traits\TimestampableTrait;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity(repositoryClass=TicketRatingRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class TicketRating implements ReviewInterface
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"myRate"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"myRate"})
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="ticketRatings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ticket;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ticketRatings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reviewer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose
     * @Groups({"myRate"})
     */
    private $comment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getReviewer(): ?User
    {
        return $this->reviewer;
    }

    public function setReviewer(?UserInterface $reviewer): self
    {
        $this->reviewer = $reviewer;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
