<?php

namespace App\Entity;

use App\Repository\EventDateRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectManagerAware;

/**
 * @ORM\Entity(repositoryClass=EventDateRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class EventDate implements ObjectManagerAware
{
    private $em;
    use TimestampableTrait;
    public function injectObjectManager(
        ObjectManager $objectManager,
        \Doctrine\Persistence\Mapping\ClassMetadata $classMetadata
    ) {
        $this->em = $objectManager;
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $end_date;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class, inversedBy="eventDates")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Exclude
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $longitude;
    /**
     * @ORM\Column(type="text")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $address;

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("city")
     * @Groups({"feedEvent"})
     * @return string
     */
    public function getCity()
    {
        if(!empty($this->getPosition())) {
            $locale = $GLOBALS['request'] ? $GLOBALS['request']->getLocale() : "ar";
            switch ($locale) {
                case "en":
                    return $this->getPosition()->getCityNameEn();
                    break;
                case "ar":
                    return $this->getPosition()->getCityNameAr();
                    break;
                default:
                    return $this->getPosition()->getCityNameFr();
                    break;
            }
        }
        return null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("country")
     * @Groups({"feedEvent"})
     * @return string
     */
    public function getCountry()
    {
        if(!empty($this->getPosition())) {
            $locale = $GLOBALS['request'] ? $GLOBALS['request']->getLocale() : "ar";
            switch ($locale) {
                case "en":
                    return $this->getPosition()->getCountryNameEn();
                    break;
                case "ar":
                    return $this->getPosition()->getCountryNameAr();
                    break;
                default:
                    return $this->getPosition()->getCountryNameFr();
                    break;
            }
        }
        return null;
    }

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="eventDates")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Exclude
     */
    private $event;

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("event_name")
     * @Groups({"new_order"})
     * @return string
     */
    public function getEventName()
    {
        return $this->getEvent() ? $this->getEvent()->getName() : null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("event_description")
     * @Groups({"new_order"})
     * @return string
     */
    public function getEventDescription()
    {
        return $this->getEvent() ? $this->getEvent()->getDescription() : null;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("event_cover")
     * @Groups({"new_order"})
     * @return string
     */
    public function getEventCover()
    {
        return $this->getEvent() ? $this->getEvent()->getCoverUrl() : null;
    }

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="eventDate")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $tickets;

    /**
     * @ORM\OneToOne(targetEntity=WithdrawalRequest::class, mappedBy="eventDate", cascade={"persist", "remove"})
     */
    private $withdrawalRequest;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setEventDate($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getEventDate() === $this) {
                $ticket->setEventDate(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getEventName()." [".$this->getStartDate()->format('Y-m-d H:i p')."]"." #".$this->getId();
    }
    public function getWithdrawalRequest(): ?WithdrawalRequest
    {
        return $this->withdrawalRequest;
    }

    public function setWithdrawalRequest(WithdrawalRequest $withdrawalRequest): self
    {
        // set the owning side of the relation if necessary
        if ($withdrawalRequest->getEventDate() !== $this) {
            $withdrawalRequest->setEventDate($this);
        }

        $this->withdrawalRequest = $withdrawalRequest;

        return $this;
    }

    public function getTotalPaidSales()
    {
        return $this->em->getRepository(Torder::class)->getTotalPaidSalesByEventDate($this)?? "0";
    }
    public function getTotalBenefit()
    {
        return (float)$this->getEvent()->getEvtickPercentage() * (float)$this->getTotalPaidSales();
    }
    public function getTotalReceivedByOwner()
    {
        return $this->getTotalPaidSales() - $this->getTotalBenefit();
    }
}
