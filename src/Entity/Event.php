<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectManagerAware;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class Event implements ObjectManagerAware
{
    private $em;
    use TimestampableTrait;
    public function injectObjectManager(
        ObjectManager $objectManager,
        \Doctrine\Persistence\Mapping\ClassMetadata $classMetadata
    ) {
        $this->em = $objectManager;
    }
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $description;


    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity=Cover::class, mappedBy="owner", cascade={"persist", "remove"})
     * @Serializer\Exclude
     */
    private $cover;

    private $coverFile;

    /**
     * @ORM\OneToMany(targetEntity=EventDate::class, mappedBy="event")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $eventDates;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $enabled;

    /**
     * @ORM\ManyToOne(targetEntity=Admin::class, inversedBy="events")
     */
    private $created_by;

    /**
     * @ORM\Column(type="float", options={"default" : 0})
     */
    private $evtickPercentage;

    /**
     * @ORM\ManyToOne(targetEntity=Admin::class)
     */
    private $owner;
    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("cover_url")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     * @return string
     */
    public function getCoverUrl()
    {
        return $this->getCover() ? $_ENV["AWS_BUCKET_URL"] . $this->getCover()->getPath() : null;
        //return $this->getCover() ? $this->getCover()->getPath() : null;

    }


    public function __construct()
    {
        $this->eventDates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getCover(): ?Cover
    {
        return $this->cover;
    }

    public function setCover(?Cover $cover): self
    {
        // unset the owning side of the relation if necessary
        if ($cover === null && $this->cover !== null) {
            $this->cover->setOwner(null);
        }

        // set the owning side of the relation if necessary
        if ($cover !== null && $cover->getOwner() !== $this) {
            $cover->setOwner($this);
        }

        $this->cover = $cover;

        return $this;
    }


    /**
     * @return Collection|EventDate[]
     */
    public function getEventDates(): Collection
    {
        return $this->eventDates;
    }

    public function addEventDate(EventDate $eventDate): self
    {
        if (!$this->eventDates->contains($eventDate)) {
            $this->eventDates[] = $eventDate;
            $eventDate->setEvent($this);
        }

        return $this;
    }

    public function removeEventDate(EventDate $eventDate): self
    {
        if ($this->eventDates->removeElement($eventDate)) {
            // set the owning side to null (unless already changed)
            if ($eventDate->getEvent() === $this) {
                $eventDate->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoverFile()
    {
        return $this->coverFile;
    }

    /**
     * @param mixed $coverFile
     */
    public function setCoverFile($coverFile): void
    {
        $this->coverFile = $coverFile;
    }


    public function __toString()
    {
        return $this->getName()." (".$this->getCategory().") "."#".$this->getId();
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedBy(): ?Admin
    {
        return $this->created_by;
    }

    public function setCreatedBy(?Admin $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getEvtickPercentage(): ?float
    {
        return $this->evtickPercentage;
    }

    public function setEvtickPercentage(float $evtickPercentage): self
    {
        $this->evtickPercentage = $evtickPercentage;

        return $this;
    }

    public function getOwner(): ?Admin
    {
        return $this->owner;
    }

    public function setOwner(?Admin $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getTotalPaidSales()
    {
        return $this->em->getRepository(Torder::class)->getTotalPaidSalesByEvent($this)?? "0";
    }
    public function getTotalBenefit()
    {
        return (float)$this->getEvtickPercentage() * (float)$this->getTotalPaidSales();
    }
    public function getTotalReceivedByOwner()
    {
        return $this->getTotalPaidSales() - $this->getTotalBenefit();
    }




}
