<?php

namespace App\Entity;

use App\Repository\PaymentCallbackRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass=PaymentCallbackRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class PaymentCallback
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idcart;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idcustomer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $op;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $achat;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTid(): ?string
    {
        return $this->tid;
    }

    public function setTid(?string $tid): self
    {
        $this->tid = $tid;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIdcart(): ?string
    {
        return $this->idcart;
    }

    public function setIdcart(?string $idcart): self
    {
        $this->idcart = $idcart;

        return $this;
    }

    public function getIdcustomer(): ?string
    {
        return $this->idcustomer;
    }

    public function setIdcustomer(?string $idcustomer): self
    {
        $this->idcustomer = $idcustomer;

        return $this;
    }

    public function getOp(): ?string
    {
        return $this->op;
    }

    public function setOp(?string $op): self
    {
        $this->op = $op;

        return $this;
    }

    public function getAchat(): ?string
    {
        return $this->achat;
    }

    public function setAchat(?string $achat): self
    {
        $this->achat = $achat;

        return $this;
    }
}
