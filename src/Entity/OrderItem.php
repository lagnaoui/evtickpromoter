<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class OrderItem
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity=Torder::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $torder;

    /**
     * @ORM\ManyToOne(targetEntity=Ticket::class, inversedBy="orderItems")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $ticket;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getTorder(): ?Torder
    {
        return $this->torder;
    }

    public function setTorder(?Torder $torder): self
    {
        $this->torder = $torder;

        return $this;
    }

    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }

    public function setTicket(?Ticket $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }
}
