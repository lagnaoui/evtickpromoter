<?php

namespace App\Entity;

use App\Entity\Traits\CommonParamsTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\VersionControlRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=VersionControlRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class VersionControl
{
    use TimestampableTrait, CommonParamsTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var enum
     *
     * @ORM\Column(name="status", type="string", columnDefinition="enum('ok', 'nok', 'opt')")
     * @Serializer\Expose
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose
     */
    private $releaseDescription;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getReleaseDescription(): ?string
    {
        return $this->releaseDescription;
    }

    public function setReleaseDescription(?string $releaseDescription): self
    {
        $this->releaseDescription = $releaseDescription;

        return $this;
    }
}
