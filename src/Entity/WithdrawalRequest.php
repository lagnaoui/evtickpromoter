<?php

namespace App\Entity;

use App\Repository\WithdrawalRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass=WithdrawalRequestRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class WithdrawalRequest
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $rib;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Fullname;

    /**
     * @ORM\OneToOne(targetEntity=EventDate::class, inversedBy="withdrawalRequest", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $eventDate;

    /**
     * @ORM\ManyToOne(targetEntity=Admin::class, inversedBy="withdrawalRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;
    /**
     * @ORM\ManyToOne(targetEntity=Admin::class, inversedBy="verifiedWithrawalRequests")
     */
    private $verifiedBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRib(): ?string
    {
        return $this->rib;
    }

    public function setRib(?string $rib): self
    {
        $this->rib = $rib;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->Fullname;
    }

    public function setFullname(?string $Fullname): self
    {
        $this->Fullname = $Fullname;

        return $this;
    }

    public function getEventDate(): ?EventDate
    {
        return $this->eventDate;
    }

    public function setEventDate(EventDate $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getOwner(): ?Admin
    {
        return $this->owner;
    }

    public function setOwner(?Admin $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
    public function getVerifiedBy(): ?Admin
    {
        return $this->verifiedBy;
    }

    public function setVerifiedBy(?Admin $verifiedBy): self
    {
        $this->verifiedBy = $verifiedBy;

        return $this;
    }
}
