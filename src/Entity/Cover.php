<?php

namespace App\Entity;

use App\Repository\CoverRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use App\Component\File as FileComponent;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=CoverRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class Cover extends FileComponent
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity=Event::class, inversedBy="cover")
     */
    protected $owner;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $path;

   public function __construct(File $file)
   {
        $this->file = $file;
   }

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("url")
     * @return string
     */
    public function getUrl()
    {
        return $_ENV["AWS_BUCKET_URL"] . $this->getPath();
    }

    public function __toString()
    {
        return $this->getPath();
    }
}
