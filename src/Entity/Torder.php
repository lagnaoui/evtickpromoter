<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class Torder
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $state;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $items_total;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $currency_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customer_ip;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="torder")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $orderItems;

    /**
     * @ORM\ManyToOne(targetEntity=Beneficiary::class, inversedBy="torder")
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $beneficiary;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     * @Serializer\Expose
     * @Groups({"new_order"})
     */
    private $rated;

    /**
     * @ORM\OneToOne(targetEntity=Tpayment::class, mappedBy="torder", cascade={"persist", "remove"})
     */
    private $tpayment;

    public function __construct()
    {
        $this->rated = false;
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getItemsTotal(): ?string
    {
        return $this->items_total;
    }

    public function setItemsTotal(string $items_total): self
    {
        $this->items_total = $items_total;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(string $currency_code): self
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    public function getCustomerIp(): ?string
    {
        return $this->customer_ip;
    }

    public function setCustomerIp(string $customer_ip): self
    {
        $this->customer_ip = $customer_ip;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setTorder($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getTorder() === $this) {
                $orderItem->setTorder(null);
            }
        }

        return $this;
    }

    public function getBeneficiary(): ?Beneficiary
    {
        return $this->beneficiary;
    }

    public function setBeneficiary(?Beneficiary $beneficiary): self
    {
        $this->beneficiary = $beneficiary;

        return $this;
    }

    public function getRated(): ?bool
    {
        return $this->rated;
    }

    public function setRated(?bool $rated): self
    {
        $this->rated = $rated;

        return $this;
    }

    public function getTpayment(): ?Tpayment
    {
        return $this->tpayment;
    }

    public function setTpayment(?Tpayment $tpayment): self
    {
        // unset the owning side of the relation if necessary
        if ($tpayment === null && $this->tpayment !== null) {
            $this->tpayment->setTorder(null);
        }

        // set the owning side of the relation if necessary
        if ($tpayment !== null && $tpayment->getTorder() !== $this) {
            $tpayment->setTorder($this);
        }

        $this->tpayment = $tpayment;

        return $this;
    }
    public function getFirstEvent()
    {
        return $this->getOrderItems()[0]->getTicket()->getEventDate()->getEvent()->getName();
    }

    public function __toString()
    {
        return $this->getNumber();
    }
}
