<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\ToggleableTrait;
use App\Repository\AdminRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=AdminRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Admin implements UserInterface
{
    use ToggleableTrait, TimestampableTrait;
    public const DEFAULT_ADMIN_ROLE = 'ROLE_ADMINISTRATION_ACCESS';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $fullName;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $secret;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $publicId;

    /**
     * @ORM\Column(type="json")
     */
    private $allowed_grant_types = [];

    /**
     * @ORM\OneToOne(targetEntity=ApiAdminToken::class, mappedBy="admin", cascade={"persist", "remove"})
     */
    private $apiAdminToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entityName;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class, inversedBy="admins")
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity=WithdrawalRequest::class, mappedBy="owner")
     */
    private $withdrawalRequests;
    /**
     * @ORM\OneToMany(targetEntity=WithdrawalRequest::class, mappedBy="verifiedBy")
     */
    private $verifiedWithrawalRequests;

    public function __construct()
    {
        $this->withdrawalRequests = new ArrayCollection();
        $this->verifiedWithrawalRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->setSecret($username);
        $this->setPublicId($username);
        $this->username = $username;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $username
     */
    public function setSecret($username): void
    {
        $this->secret = md5(uniqid("secret".$username, true));
    }

    /**
     * @return mixed
     */
    public function getAllowedGrantTypes()
    {
        return $this->allowed_grant_types;
    }

    /**
     * @param mixed $allowed_grant_types
     */
    public function setAllowedGrantTypes($allowed_grant_types): void
    {
        $this->allowed_grant_types = $allowed_grant_types;
    }


    public function __toString(): string
    {
        return $this->username;
    }

    public function getApiAdminToken(): ?ApiAdminToken
    {
        return $this->apiAdminToken;
    }

    public function setApiAdminToken(ApiAdminToken $apiAdminToken): self
    {
        $this->apiAdminToken = $apiAdminToken;

        // set the owning side of the relation if necessary
        if ($apiAdminToken->getAdmin() !== $this) {
            $apiAdminToken->setAdmin($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getPublicId()
    {
        return $this->publicId;
    }

    /**
     * @param mixed $username,
     */
    public function setPublicId($username): void
    {
        $this->publicId = md5(uniqid("id".$username, true));
    }

    public function getEntityName(): ?string
    {
        return $this->entityName;
    }

    public function setEntityName(string $entityName): self
    {
        $this->entityName = $entityName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }
    /**
     * @return Collection|WithdrawalRequest[]
     */
    public function getWithdrawalRequests(): Collection
    {
        return $this->withdrawalRequests;
    }

    public function addWithdrawalRequest(WithdrawalRequest $withdrawalRequest): self
    {
        if (!$this->withdrawalRequests->contains($withdrawalRequest)) {
            $this->withdrawalRequests[] = $withdrawalRequest;
            $withdrawalRequest->setOwner($this);
        }

        return $this;
    }

    public function removeWithdrawalRequest(WithdrawalRequest $withdrawalRequest): self
    {
        if ($this->withdrawalRequests->removeElement($withdrawalRequest)) {
            // set the owning side to null (unless already changed)
            if ($withdrawalRequest->getOwner() === $this) {
                $withdrawalRequest->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WithdrawalRequest[]
     */
    public function getVerifiedWithrawalRequests(): Collection
    {
        return $this->verifiedWithrawalRequests;
    }

    public function addVerifiedWithrawalRequest(WithdrawalRequest $verifiedWithrawalRequest): self
    {
        if (!$this->verifiedWithrawalRequests->contains($verifiedWithrawalRequest)) {
            $this->verifiedWithrawalRequests[] = $verifiedWithrawalRequest;
            $verifiedWithrawalRequest->setVerifiedBy($this);
        }

        return $this;
    }

    public function removeVerifiedWithrawalRequest(WithdrawalRequest $verifiedWithrawalRequest): self
    {
        if ($this->verifiedWithrawalRequests->removeElement($verifiedWithrawalRequest)) {
            // set the owning side to null (unless already changed)
            if ($verifiedWithrawalRequest->getVerifiedBy() === $this) {
                $verifiedWithrawalRequest->setVerifiedBy(null);
            }
        }

        return $this;
    }

}
