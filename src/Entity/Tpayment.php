<?php

namespace App\Entity;

use App\Repository\TpaymentRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass=TpaymentRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Tpayment
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $payment_method;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $purchase;

    /**
     * @ORM\OneToOne(targetEntity=Torder::class, inversedBy="tpayment", cascade={"persist", "remove"})
     */
    private $torder;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tid;

    public function getId(): ?int
    {
        return $this->id;
    }



    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->payment_method;
    }

    public function setPaymentMethod(string $payment_method): self
    {
        $this->payment_method = $payment_method;

        return $this;
    }

    public function getPurchase(): ?string
    {
        return $this->purchase;
    }

    public function setPurchase(?string $purchase): self
    {
        $this->purchase = $purchase;

        return $this;
    }

    public function getTorder(): ?Torder
    {
        return $this->torder;
    }

    public function setTorder(?Torder $torder): self
    {
        $this->torder = $torder;

        return $this;
    }

    public function getTid(): ?string
    {
        return $this->tid;
    }

    public function setTid(?string $tid): self
    {
        $this->tid = $tid;

        return $this;
    }

    public function __toString()
    {
        return $this->getTid();
    }

}
