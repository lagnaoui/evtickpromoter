<?php

namespace App\Entity;

use App\Entity\interfaces\ReviewableInterface;
use App\Repository\TicketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use App\Entity\Traits\TimestampableTrait;

/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @Serializer\ExclusionPolicy("ALL")
 */
class Ticket implements ReviewableInterface
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $init_quantity;

    /**
     * @ORM\ManyToOne(targetEntity=EventDate::class, inversedBy="tickets")
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $eventDate;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="ticket")
     */
    private $orderItems;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose
     * @Groups({"feedEvent", "new_order"})
     */
    private $currency_code;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     * @Serializer\Expose
     * @Groups({"feedEvent"})
     */
    private $quantity_on_hold;

    /**
     * @ORM\OneToMany(targetEntity=TicketRating::class, mappedBy="ticket", orphanRemoval=true)
     */
    private $ticketRatings;

    /**
     * @ORM\Column(type="float", options={"default" : 0})
     */
    private $averageRating;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $numberOfRaters;

    public function __construct()
    {
        $this->quantity_on_hold = 0;
        $this->averageRating = 0;
        $this->numberOfRaters =0;
        $this->orderItems = new ArrayCollection();
        $this->ticketRatings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInitQuantity()
    {
        return $this->init_quantity;
    }

    /**
     * @param mixed $init_quantity
     */
    public function setInitQuantity($init_quantity): void
    {
        $this->init_quantity = $init_quantity;
    }

    public function getEventDate(): ?EventDate
    {
        return $this->eventDate;
    }

    public function setEventDate(?EventDate $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setTicket($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getTicket() === $this) {
                $orderItem->setTicket(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    public function setCurrencyCode(string $currency_code): self
    {
        $this->currency_code = $currency_code;

        return $this;
    }

    public function getQuantityOnHold(): ?int
    {
        return $this->quantity_on_hold;
    }

    public function setQuantityOnHold(int $quantity_on_hold): self
    {
        $this->quantity_on_hold = $quantity_on_hold;

        return $this;
    }

    /**
     * @return Collection|TicketRating[]
     */
    public function getTicketRatings(): Collection
    {
        return $this->ticketRatings;
    }

    public function addTicketRating(TicketRating $ticketRating): self
    {
        if (!$this->ticketRatings->contains($ticketRating)) {
            $this->ticketRatings[] = $ticketRating;
            $ticketRating->setTicket($this);
        }

        return $this;
    }

    public function removeTicketRating(TicketRating $ticketRating): self
    {
        if ($this->ticketRatings->removeElement($ticketRating)) {
            // set the owning side to null (unless already changed)
            if ($ticketRating->getTicket() === $this) {
                $ticketRating->setTicket(null);
            }
        }

        return $this;
    }

    public function getAverageRating(): ?float
    {
        return $this->averageRating;
    }

    public function setAverageRating(?float $averageRating): self
    {
        $this->averageRating = $averageRating;

        return $this;
    }

    public function getNumberOfRaters(): ?int
    {
        return $this->numberOfRaters;
    }

    public function setNumberOfRaters(?int $numberOfRaters): self
    {
        $this->numberOfRaters = $numberOfRaters;

        return $this;
    }

    /**
     * @return Collection|TicketRating[]
     */
    public function getReviews(): Collection
    {
        return $this->ticketRatings;
    }
    public function addReview($ticketRating)
    {
        if (!$this->ticketRatings->contains($ticketRating)) {
            $this->ticketRatings[] = $ticketRating;
            $ticketRating->setTicket($this);
        }

        return $this;
    }

    public function removeReview($ticketRating): self
    {
        if ($this->ticketRatings->removeElement($ticketRating)) {
            // set the owning side to null (unless already changed)
            if ($ticketRating->getTicket() === $this) {
                $ticketRating->setTicket(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getEventDate()."(".$this->getType().")";
    }

}
