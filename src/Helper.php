<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 1/4/21
 * Time: 17:05
 */

namespace App;


use App\Business\BusinessAdmin;
use App\Entity\Admin;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class Helper
{
    private $ba;
    private $base_url;
    public function __construct(BusinessAdmin $ba)
    {
        $this->ba = $ba;
        $this->base_url = $_SERVER["BASE_URL_API"];
    }


    public function callApi(Admin $user, $uri, $data, $method = "PUT")
    {
        /* @var $user Admin */
        $user = $this->ba->refreshAdmin($user);
        $url = $this->base_url . $uri;
        $result = $this->httpRequest($url, $data, $method, $this->getHeaders($user->getApiAdminToken()->getAccessToken()));
        if (401 == $result[0]) {
            $resultRef = $this->refreshChefyApi($user);
            if (200 == $resultRef[0]) {
                return $this->httpRequest($url, $data, $method, $this->getHeaders($resultRef[1]["body"]["access_token"]))[1];
            }
            return ["message"=>"LOG_OUT"];
        }
        return $result[1];
    }

    public function authApi(Admin $user)
    {
        $url = $this->base_url . "auth/token";
        $data = ["username" => $user->getUsername(), "secret" => $user->getSecret(), "public_id" => $user->getPublicId()];
        return $this->httpRequest($url, $data, "POST");
    }

    public function refreshApi(Admin $user)
    {
        $url = $this->base_url . "auth/token/refresh";
        $data = ["public_id" => $user->getPublicId()];
        return $this->httpRequest($url, $data, "POST", $this->getHeaders(null, $user->getApiAdminToken()->getRefreshToken()));
    }

    /**
     * @param $url
     * @param $data
     * @param $method
     * @param array $header
     * @param bool $ssl
     * @return array|bool
     */
    public function httpRequest($url, $data, $method, $header = ["accept" => "application/json"], $ssl = false)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (!$response) {
            return false;
        }
        return [$httpcode, json_decode($response, true)];
    }

    private function getHeaders($authToken = null, $authRefresh = null)
    {
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        if ($authToken) {
            $headers[] = 'X-Auth-Token: ' . $authToken;
        }
        if ($authRefresh) {
            $headers[] = 'X-REF-TOKEN: ' . $authRefresh;
        }
        return $headers;

    }
}
