<?php

namespace App\Form;

use App\Entity\Position;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country_code')
            ->add('country_name_en')
            ->add('country_name_ar')
            ->add('country_name_fr')
            ->add('city_id')
            ->add('city_name_en')
            ->add('city_name_ar')
            ->add('city_name_fr')
            ->add('latitude')
            ->add('longitude')
            ->add('postal_code')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Position::class,
        ]);
    }
}
