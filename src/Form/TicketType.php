<?php

namespace App\Form;

use App\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('price')
            ->add('quantity')
            ->add('init_quantity')
            ->add('color')
            ->add('currency_code')
            ->add('quantity_on_hold')
            ->add('averageRating')
            ->add('numberOfRaters')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('eventDate')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
