<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 12/17/21
 * Time: 16:08
 */

namespace App\Admin\Field;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

final class CustomImageField implements FieldInterface
{
    use FieldTrait;


    public static function new(string $propertyName, ?string $label=null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)

            // this template is used in 'index' and 'detail' pages
            ->setTemplatePath('cover.html.twig')

            // this is used in 'edit' and 'new' pages to edit the field contents
            // you can use your own form types too
            //->setFormType(TextareaType::class)
            ->addCssClass('field-cover')
            ;
    }
}
