<?php

namespace App\Repository;

use App\Entity\PaymentCallback;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentCallback|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentCallback|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentCallback[]    findAll()
 * @method PaymentCallback[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentCallbackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentCallback::class);
    }

    // /**
    //  * @return PaymentCallback[] Returns an array of PaymentCallback objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentCallback
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
