<?php

namespace App\Repository;

use App\Entity\VersionControl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VersionControl|null find($id, $lockMode = null, $lockVersion = null)
 * @method VersionControl|null findOneBy(array $criteria, array $orderBy = null)
 * @method VersionControl[]    findAll()
 * @method VersionControl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VersionControlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VersionControl::class);
    }

    // /**
    //  * @return VersionControl[] Returns an array of VersionControl objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VersionControl
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
