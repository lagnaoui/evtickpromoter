<?php

namespace App\Repository;

use App\Entity\TicketRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TicketRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method TicketRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method TicketRating[]    findAll()
 * @method TicketRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TicketRating::class);
    }

    // /**
    //  * @return TicketRating[] Returns an array of TicketRating objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicketRating
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
