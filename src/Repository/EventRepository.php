<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\Position;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Event::class);
        $this->paginator = $paginator;
    }

    public function findEventBy(?string $country_code, ?int $city_id, ?string $name, ?string $category, ?int $page = 1, ?int $limit = 10)
    {
        $qb = $this->createQueryBuilder('e')
            //->where("e.start_date >= :today")
            //->setParameter('today', new \DateTime())
              //->join(EventDate::class, 'ev', 'WITH', 'ev.event = e.id')
        ;

        if(!empty($country_code) && !empty($city_id))
        {
            $qb = $qb->Join(Position::class, 'p', 'WITH', 'p.id = ev.position');
            $qb = $qb->andWhere('p.country_code LIKE :country_code AND p.city_id LIKE :city_id')
                ->setParameters(['country_code'=>$country_code, 'city_id'=>$city_id]);
        }


        if(!empty($name))
        {
            $qb = $qb->andWhere('e.name LIKE :name')
                ->setParameter('name', "%$name%");
        }

        if(!empty($category))
        {
            $qb = $qb->andWhere('e.category LIKE :category')
            ->setParameter('category', $category);

        }
        $qb = $qb->orderBy("e.id", "ASC");

        $query = $qb->getQuery();

        return $this->paginator->paginate(
            $query,
            isset($page) ? $page : 1,
            isset($limit) ? $limit : 10
        );
    }



    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
