<?php

namespace App\Repository;

use App\Entity\Tpayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tpayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tpayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tpayment[]    findAll()
 * @method Tpayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TpaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tpayment::class);
    }

    // /**
    //  * @return Tpayment[] Returns an array of Tpayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tpayment
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
