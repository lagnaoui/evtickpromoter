<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\OrderItem;
use App\Entity\Ticket;
use App\Entity\Torder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Torder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Torder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Torder[]    findAll()
 * @method Torder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Torder::class);
        $this->paginator = $paginator;
    }

    public function findOrdersBy($user, string $state, ?int $page = 1, ?int $limit = 10)
    {
        $qb = $this->createQueryBuilder('o')
        ->andWhere('o.user = :user')->setParameter('user', $user);

        if($state == "all_unpaid")
        {
            $qb = $qb->andWhere('o.state NOT LIKE :state')
                ->setParameter('state', 'payment_OK');

        }else
        {
            $qb = $qb->andWhere('o.state LIKE :state')
                ->setParameter('state', $state);
        }
        $qb = $qb->orderBy("o.id", "DESC");

        $query = $qb->getQuery();

        return $this->paginator->paginate(
            $query,
            isset($page) ? $page : 1,
            isset($limit) ? $limit : 10
        );
    }
    public function findOrdersToReview($user)
    {
        $qb = $this->createQueryBuilder('o')
                ->andWhere('o.user = :user')
                ->andWhere('o.state LIKE :state')
                ->andWhere('o.rated = :rated')
                ->setParameters(['user'=> $user, 'state'=> 'payment_ok', 'rated'=>0])
            ;
        $query = $qb->getQuery();
        return $query->getResult();
    }

    public function getTotalPaidSales()
    {
        return  (float)$this->createQueryBuilder('o')
            ->select('SUM(o.items_total)')
            ->andWhere('o.state = :state')
            ->setParameter('state', 'payment_ok')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function countPaid()
    {
        return (int) $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->andWhere('o.state = :state')
            ->setParameter('state', 'payment_ok')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function findLatest(int $count)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.state != :state')
            ->addOrderBy('o.createdAt', 'DESC')
            ->setParameter('state', 'new')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult()
            ;
    }

    public function countSalesByMonth(int $year = 2021)
    {
        $result = (int)$this->createQueryBuilder('o')
            ->select("MONTH(o.createdAt) as month, SUM(o.items_total) as sales")
            ->andWhere("YEAR(o.createdAt) = :year")
            ->andWhere("o.state = :state")
            ->addGroupBy("month")
            ->setParameter("year", $year)
            ->setParameter('state', 'payment_ok')
            ->getQuery()->getResult();
        return $result;
    }

    public function getTotalPaidSalesByOwner($owner)
    {
        return (float) $this->createQueryBuilder('o')
            ->select('SUM(o.items_total)')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->andWhere('o.state = :state')
            ->setParameters(['state'=>'payment_ok', 'owner'=>$owner])
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function getTotalPaidSalesByOwnerPlatformFees($owner)
    {
        return (float) $this->createQueryBuilder('o')
            ->select('SUM(o.items_total*e.evtickPercentage)')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->andWhere('o.state = :state')
            ->setParameters(['state'=>'payment_ok', 'owner'=>$owner])
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
    public function countPaidByOwner($owner)
    {
        return (int) $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->andWhere('o.state = :state')
            ->setParameters(['state'=>'payment_ok', 'owner'=>$owner])
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function findLatestByOwner(int $count, $owner)
    {
        return $this->createQueryBuilder('o')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->andWhere('o.state != :state')
            ->addOrderBy('o.createdAt', 'DESC')
            ->setParameters(['state'=>'new', 'owner'=>$owner])
            ->setMaxResults($count)
            ->getQuery()
            ->getResult()
            ;
    }

    public function countSalesByMonthByOwner(int $year, $owner)
    {
        $result = $this->createQueryBuilder('o')
            ->select("MONTH(o.createdAt) as month, SUM(o.items_total) as sales")
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->andWhere("YEAR(o.createdAt) = :year")
            ->andWhere("o.state = :state")
            ->addGroupBy("month")
            ->setParameter("year", $year)
            ->setParameter("state", 'payment_ok')
            ->setParameter("owner", $owner)
            ->getQuery()->getResult();
        return $result;
    }

    public function getTotalPaidSalesByEvent($event)
    {
        return  $this->createQueryBuilder('o')
            ->select('SUM(o.items_total)')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.id = :event')
            ->andWhere('o.state = :state')
            ->setParameters(['state'=>'payment_ok', 'event'=>$event])
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    public function getTotalPaidSalesByEventDate($eventDate)
    {
        return $this->createQueryBuilder('o')
            ->select('SUM(o.items_total)')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->andWhere('ed.id = :eventDate')
            ->andWhere('o.state = :state')
            ->setParameters(['state'=>'payment_ok', 'eventDate'=>$eventDate])
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }



    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
