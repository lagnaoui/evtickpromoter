<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\OrderItem;
use App\Entity\Position;
use App\Entity\Ticket;
use App\Entity\Torder;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
    public function countCustomers(): int
    {
        return (int) $this->createQueryBuilder('o')
            ->select('COUNT(o.id)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
    public function findLatest(int $count): array
    {
        return $this->createQueryBuilder('o')
            ->addOrderBy('o.createdAt', 'DESC')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult()
            ;
    }
    public function getUsersByMonthStats(int $year = 2022)
    {
        $result = $this->createQueryBuilder('u')
            ->select("MONTH(u.createdAt) as month, count(u) as users")
            ->andWhere("YEAR(u.createdAt) = :year")
            ->andWhere("u.enabled = true")
            ->addGroupBy("month")
            ->setParameter("year", $year)
            ->getQuery()->getResult();
        return $result;
    }

    public function countCustomersByOwner($user): int
    {
        return (int) $this->createQueryBuilder('u')
            ->select('COUNT(DISTINCT u.id)')
            ->leftJoin(Torder::class, 'o', 'WITH', 'o.user = u.id')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->setParameter('owner', $user)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
    public function findLatestByOwner(int $count, $user): array
    {
        return $this->createQueryBuilder('u')
            ->leftJoin(Torder::class, 'o', 'WITH', 'o.user = u.id')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->setParameter('owner', $user)
            ->addOrderBy('u.createdAt', 'DESC')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult()
            ;
    }


    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
