<?php

namespace App\Repository;

use App\Entity\ApiAdminToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiAdminToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiAdminToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiAdminToken[]    findAll()
 * @method ApiAdminToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiAdminTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApiAdminToken::class);
    }

    // /**
    //  * @return ApiAdminToken[] Returns an array of ApiAdminToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApiAdminToken
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
