<?php

namespace App\Repository;

use App\Entity\Caterer;
use App\Entity\Customer;
use App\Entity\Device;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 * @method Device|null findOneBy(array $criteria, array $orderBy = null)
 * @method Device[]    findAll()
 * @method Device[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Device::class);
    }

    // /**
    //  * @return Device[] Returns an array of Device objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findDevicesUsersBy(?string $userType, ?string $status, string $platform = "ios")
    {
        $qb =  $this->createQueryBuilder('d')
                ->select("u.id")
                ->join('d.user', 'u')
                ->andWhere("d.token is not null");

        if(isset($platform)) {
            $qb = $qb->andWhere('d.os = :platform')
                ->setParameter('platform', $platform);
        }

        if(isset($userType)) {
            $type = $userType == "customer" ? Customer::class : Caterer::class;
            $qb = $qb->andWhere($qb->expr()->isInstanceOf('u', $type));
        }

        if(isset($status)) {
            $qb = $qb->andWhere('u.enabled = :isEnabled')
                ->setParameter('isEnabled', $status === "enabled");
        }
        return $qb->getQuery()->getArrayResult();
    }

    public function getUsersTokens(array $usersIds)
    {
        $qb =  $this->createQueryBuilder('d')
            ->select("d.token as token, d.os as platform")
            ->innerJoin('d.user', 'u')
            ->andWhere("u.id IN (:ids)")
            ->setParameter('ids', $usersIds);;
        return $qb->getQuery()->getArrayResult();
    }

}
