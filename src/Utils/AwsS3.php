<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/4/20
 * Time: 16:35
 */

namespace App\Utils;


use Symfony\Component\HttpFoundation\File\File;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3UriParser;

class AwsS3
{
    private static $s3Client;
    private $s3;

    public function __construct()
    {
        $this->s3 = $this->getClientS3Instance();
    }

    /**
     * @return S3Client
     */
    private function getClientS3Instance()
    {
        if (is_null(self::$s3Client)) {
            self::$s3Client = new S3Client(['version' => $_SERVER["AWS_VERSION"],
                'region' => $_SERVER["AWS_REGION"],
                /*'credentials' => [
                    'key' => $_SERVER["AWS_KEY"],
                    'secret' => $_SERVER["AWS_SECRET"]
                ]*/
            ]);
        }
        return self::$s3Client;
    }

    /**
     * @param File $file
     * @param $key
     * @return String
     */
    public function uploadFileToS3(File $file, string $key)
    {
        try {
            return $this->s3->putObject(["Bucket" => $_SERVER["AWS_BUCKET"],
                "Key" => $key,
                "Body" => fopen($file, 'rb'),
                "ACL" => $_SERVER["AWS_PUBLIC_READ"],
            ]);
        } catch (S3Exception $exception) {
            $msg = $exception->getMessage();
            return false;
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function deleteObjectFromS3(string $key)
    {
        try {
            $result = $this->s3->deleteObject([
                'Bucket' => $_SERVER["AWS_BUCKET"],
                'Key' => $key
            ]);
            return $result['DeleteMarker'] ? true : false;
        } catch (S3Exception $ex) {
            $msg = $ex->getMessage();
            return false;
        }
    }


}