<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 1/9/22
 * Time: 23:35
 */

namespace App\Filter;


use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\OrderItem;
use App\Entity\Ticket;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\TextFilterType;

class EventFilter implements FilterInterface
{

    use FilterTrait;

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(TextFilterType::class);
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {

        $queryBuilder
            ->andWhere('e.id = :param01 OR e.name LIKE :param01 ')
            ->setParameter('param01', $filterDataDto->getValue());

    }

}
