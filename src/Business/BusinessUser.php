<?php

namespace App\Business;

use App\Entity\Admin;
use App\Helper;

class BusinessUser
{
    private $helper;

    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    public function deleteUser(Admin $user, $resourceId, $uri = "users")
    {
        $data = ["userId" => $resourceId];
        $response = $this->helper->callApi($user, $uri, $data, "DELETE");
        return $response["message"];
    }
}
