<?php


namespace App\Business;


use App\Entity\Device;
use App\Entity\Torder;
use App\Entity\User;
use App\Helper;
use Doctrine\ORM\EntityManagerInterface;

class BusinessDashboard
{
    private $helper;
    private $em;

    public function __construct(Helper $helper, EntityManagerInterface $em)
    {
        $this->helper = $helper;
        $this->em = $em;
    }

    public function getUsersStats()
    {
        return $this->em->getRepository(User::class)->getUsersCountStats();

    }

    public function getDevicesStats()
    {
        return $this->em->getRepository(Device::class)->getDeviceCountStats();
    }

    public function getGraphStats(int $year = 2021)
    {
        $monthsStats = $this->em->getRepository(User::class)->getUsersByMonthStats($year);
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $numUsers = [];
        foreach ($months as $month) {
            $key = array_search($month, array_column($monthsStats, 'month'));
            if ($key !== false) {
                $numUsers[] = $monthsStats[$key]["users"];
            } else {
                $numUsers[] = 0;
            }
        }
        return ["months" => $months, "users" => $numUsers];
    }

    public function getGraphSales(int $year, $owner)
    {
        $monthsStats = $this->em->getRepository(Torder::class)->countSalesByMonthByOwner($year, $owner->getId());
        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        $sales = [];
        foreach ($months as $month) {
            $key = array_search($month, array_column($monthsStats, 'month'));
            if ($key !== false) {
                $sales[] = (float)$monthsStats[$key]["sales"];
            } else {
                $sales[] = 0;
            }
        }
        return ["months" => $months, "sales" => $sales];
    }

    public function getStatsInfo($user): array
    {
        $totalSales = $this->em->getRepository(Torder::class)->getTotalPaidSalesByOwner($user->getId());
        $totalSalesPlatformFees = $this->em->getRepository(Torder::class)->getTotalPaidSalesByOwnerPlatformFees($user->getId());
        $countPaidOrder = $this->em->getRepository(Torder::class)->countPaidByOwner($user->getId());
        $latestOrders = $this->em->getRepository(Torder::class)->findLatestByOwner(3, $user->getId());
        $countCustomers = $this->em->getRepository(User::class)->countCustomersByOwner($user->getId());
        $latestCustomers = $this->em->getRepository(User::class)->findLatestByOwner(3, $user->getId());
        return [
            "totalSales"=>$totalSales,
            "totalSalesPlatformFees"=>$totalSalesPlatformFees,
            "countPaidOrder"=>$countPaidOrder,
            "latestOrders"=>$latestOrders,
            "countCustomers" => $countCustomers,
            "latestCustomers" => $latestCustomers,
            "graphSales"=>$this->getGraphSales(2021, $user),
            "graphSales2022" => $this->getGraphSales(2022, $user),
        ];
    }
}
