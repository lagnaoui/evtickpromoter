<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 1/6/21
 * Time: 20:05
 */

namespace App\Business;


use App\Entity\Admin;
use Doctrine\ORM\EntityManagerInterface;

class BusinessAdmin
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function refreshAdmin(Admin $admin)
    {
        return $this->em->getRepository(Admin::class)->find($admin->getId());
    }

}
