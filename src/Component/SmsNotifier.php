<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/4/20
 * Time: 22:48
 */

namespace App\Component;


use App\Utils\TwilioSms;
use Psr\Log\LoggerInterface;

class SmsNotifier implements SmsNotifierInterface
{
    private $tw;
    private $logger;

    public function __construct(TwilioSms $tw, LoggerInterface $smsLogger)
    {
        $this->tw = $tw;
        $this->logger = $smsLogger;
    }

    public function send(string $phone, string $code)
    {
        if ($_SERVER["APP_HOST"] == "dev") {
            $this->logger->info("dev $code to $phone");
            return false;
        } else {
            $result = $this->tw->sendSMS($phone, $code);
            $this->logger->info(json_encode($result));
            return $result;
        }
    }

}
