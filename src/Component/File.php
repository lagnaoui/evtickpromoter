<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/1/20
 * Time: 17:12
 */

namespace App\Component;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * @Serializer\ExclusionPolicy("ALL")
 */
abstract class File implements FileInterface
{
    /** @var mixed */
    protected $id;

    protected $type;

    /** @var \SplFileInfo|null */
    protected $file;

    protected $path;

    /** @var object|null */
    protected $owner;

    public function getId()
    {
        return $this->id;
    }


    public function getType(): ?string
    {
        return $this->type;
    }
    
    public function setType(?string $type): void
    {
        $this->type = $type;
    }


    public function getFile(): ?\SplFileInfo
    {
        return $this->file;
    }


    public function setFile(?\SplFileInfo $file): void
    {
        $this->file = $file;
    }


    public function hasFile(): bool
    {
        return null !== $this->file;
    }

 
    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): void
    {
        $this->path = $path;
    }

    public function hasPath(): bool
    {
        return null !== $this->path;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }
    public function getUri()
    {
        return $_ENV["AWS_BUCKET_URL"];
    }

}
