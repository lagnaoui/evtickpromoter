<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/3/20
 * Time: 13:42
 */

namespace App\Component;

use App\Utils\AwsS3;
use Symfony\Component\HttpFoundation\File\File;
use Webmozart\Assert\Assert;

class FileUploader implements FileUploaderInterface
{
    /** @var FilePathGeneratorInterface */
    protected $filePathGenerator;
    protected $awsS3;

    public function __construct(FilePathGeneratorInterface $filePathGenerator, AwsS3 $s3)
    {
        $this->filePathGenerator = $filePathGenerator;
        $this->awsS3 = $s3;
    }

    public function upload(FileInterface $file, string $folderName): void
    {
        if (!$file->hasFile()) {
            return;
        }
        $splFile = $file->getFile();
        Assert::isInstanceOf($splFile, File::class);
        if (null !== $file->getPath()) {
            $this->remove($file->getPath());
        }
        $path = $this->filePathGenerator->generate($file, $folderName);
        $file->setPath($path);
        $this->awsS3->uploadFileToS3($file->getFile(), $file->getPath());
    }

    /**
     * {@inheritdoc}
     */
    public function remove(string $path): bool
    {
        return $this->awsS3->deleteObjectFromS3($path);
    }

}
