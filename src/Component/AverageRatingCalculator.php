<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/16/20
 * Time: 17:48
 */

namespace App\Component;


use App\Entity\interfaces\ReviewableInterface;

class AverageRatingCalculator implements AverageRatingCalculatorInterface
{
    public function calculate(ReviewableInterface $reviewable): float
    {
        $sum = 0;
        $reviewsNumber = 0;
        $reviews = $reviewable->getReviews();
        foreach ($reviews as $review) {
                ++$reviewsNumber;
                $sum += $review->getRating();
        }
        return 0 !== $reviewsNumber ? $sum / $reviewsNumber : 0;
    }
    public function getReviewsNumber(ReviewableInterface $reviewable): int
    {
        return count($reviewable->getReviews());
    }

}
