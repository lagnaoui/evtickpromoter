<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 12/11/20
 * Time: 16:44
 */

namespace App\Component;


use App\Helper\Helper;
use Psr\Log\LoggerInterface;
use Pushok\Client;
use Pushok\Notification;
use Pushok\Payload;
use Pushok\Payload\Alert;
use Pushok\AuthProvider;


class PushNotifier implements PushNotifierInterface
{
    private $fcm_access_key;
    private $fcm_url;
    private $logger;
    private $ios_key_id;
    private $ios_team_id;
    private $ios_app_bundle_id;
    private $ios_apns_private_key_path;
    private $ios_apns_private_key_secret;
    private $is_prod_ios;

    public function __construct(string $fcm_access_key, string $fcm_url, $ios_key_id, $ios_team_id, $ios_app_bundle_id, $ios_apns_private_key_path, $ios_apns_private_key_secret, bool $is_prod_ios, LoggerInterface $pushLogger)
    {
        $this->fcm_access_key = $fcm_access_key;
        $this->fcm_url = $fcm_url;
        $this->logger = $pushLogger;
        $this->ios_key_id = $ios_key_id;
        $this->ios_team_id = $ios_team_id;
        $this->ios_app_bundle_id = $ios_app_bundle_id;
        $this->ios_apns_private_key_path = $ios_apns_private_key_path;
        $this->ios_apns_private_key_secret = $ios_apns_private_key_secret;
        $this->is_prod_ios = $is_prod_ios;
    }

    /**
     * @param array $tokens
     * @param string $title
     * @param string $message
     * @param array|null $data
     */
    public function pushAndroid(array $tokens, string $title = "title message", string $message = "notification message", array $data = null): void
    {
        $this->logger->info("push notification ANDROID...");
        $headers = [
            'Authorization: key=' . $this->fcm_access_key,
            'Content-Type: application/json'
        ];
        if (is_null($data)) {
            $fields = [
                'registration_ids' => $tokens,
                'notification' => [
                    'body' => $message,
                    'title' => $title,
                    'icon' => 'myicon',
                    'sound' => 'mySound',
                    "tag" => "1"
                ]
            ];

        } else {
            $fields = [
                'registration_ids' => $tokens,
                'data' => [
                    'body' => $data,
                    'title' => $title,
                    'message' => $message,
                    'icon' => 'myicon',
                    'sound' => 'mySound'
                ]
            ];

        }
        $res = Helper::curl_request(["url" => $this->fcm_url, "headers" => $headers, "fields" => $fields]);
        $this->logger->info("push notification ...");
        $this->logger->info(json_encode($fields));
        $this->logger->info(json_encode($res));
        return;
    }

    /**
     * @param array $deviceTokens
     * @param string $title
     * @param string $message
     * @param array|null $data
     * @throws \Pushok\InvalidPayloadException
     */
    public function pushIos(array $deviceTokens, string $title = "title message", string $message = "notification message", array $data = null): void
    {
        $this->logger->info("push notification IOS...");
        $options = [
            'key_id' => $this->ios_key_id,
            'team_id' => $this->ios_team_id,
            'app_bundle_id' => $this->ios_app_bundle_id,
            'private_key_path' => $this->ios_apns_private_key_path,
            'private_key_secret' => $this->ios_apns_private_key_secret
        ];
        $authProvider = AuthProvider\Token::create($options);
        $alert = Alert::create()->setTitle($title);
        $alert = $alert->setBody($message);
        $payload = Payload::create()->setAlert($alert);
        $payload->setSound('default');
        $payload->setBadge(1);
        if($data)
        foreach ($data as $d => $dValue) {
            $payload->setCustomValue($d, $dValue);

        }
        $notifications = [];
        foreach ($deviceTokens as $deviceToken) {
            $this->logger->info("Device Token : ".$deviceToken);
            if($deviceToken)
            $notifications[] = new Notification($payload, $deviceToken);
        }
        $client = new Client($authProvider, $this->is_prod_ios);
        $client->addNotifications($notifications);
        $responses = $client->push();
        $this->logger->info(json_encode($responses));
        foreach ($responses as $response)
        {
            $this->logger->info("Response apn: id: " . $response->getApnsId() . " code: " . $response->getStatusCode() . " phrase :" . $response->getReasonPhrase() . " error reason: " . $response->getErrorReason() . "error description: " . $response->getErrorDescription());
        }
        return;
    }

}
