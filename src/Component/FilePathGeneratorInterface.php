<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/3/20
 * Time: 13:53
 */

namespace App\Component;

interface FilePathGeneratorInterface
{
    public function generate(FileInterface $file, string $folderName): string;
}
