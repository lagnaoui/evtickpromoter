<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 12/11/20
 * Time: 16:45
 */
namespace App\Component;

interface PushNotifierInterface
{
    public function pushAndroid(array $tokens, string $title, string $message, array $data=null): void ;
    public function pushIos(array $tokens, string $title, string $message, array  $data=null):  void ;
}
