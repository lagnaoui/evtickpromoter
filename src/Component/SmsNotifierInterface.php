<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/4/20
 * Time: 22:32
 */

namespace App\Component;

interface SmsNotifierInterface
{
    public function send(string $phone, string $code);
}

