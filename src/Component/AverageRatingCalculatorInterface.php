<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/16/20
 * Time: 23:37
 */

namespace App\Component;

use App\Entity\interfaces\ReviewableInterface;

interface AverageRatingCalculatorInterface
{
    public function calculate(ReviewableInterface $reviewable): float;
}
