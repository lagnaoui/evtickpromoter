<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/1/20
 * Time: 17:09
 */

namespace App\Component;

interface FileInterface
{
    /**
     * @return string
     */
    public function getType(): ?string;

    public function setType(?string $type): void;

    public function getFile(): ?\SplFileInfo;

    public function setFile(?\SplFileInfo $file): void;

    public function hasFile(): bool;

    public function getPath(): ?string;

    public function setPath(?string $path): void;

    /**
     * @return object
     */
    public function getOwner();

    /**
     * @param object|null $owner
     */
    public function setOwner($owner): void;
}