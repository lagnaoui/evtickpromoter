<?php
/**
 * Created by PhpStorm.
 * User: soufianlagnaoui
 * Date: 9/3/20
 * Time: 13:36
 */

namespace App\Component;

interface FileUploaderInterface
{
    public function upload(FileInterface $file, string $folderName): void;

    public function remove(string $path): bool;
}