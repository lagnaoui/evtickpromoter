<?php

namespace App\Controller\Admin;

use App\Business\BusinessDashboard;
use App\Entity\Admin;
use App\Entity\Beneficiary;
use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\PaymentCallback;
use App\Entity\Session;
use App\Entity\SmsVerification;
use App\Entity\Ticket;
use App\Entity\TicketRating;
use App\Entity\Torder;
use App\Entity\Tpayment;
use App\Entity\User;
use App\Entity\VersionControl;
use App\Entity\Device;
use App\Entity\WithdrawalRequest;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    private $businessDashboard;

    public function __construct(BusinessDashboard $businessDashboard)
    {
        $this->businessDashboard = $businessDashboard;
    }
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('dashboard.html.twig',
                                    array_merge([
                                        "currencyCode"=>"CFA"
                                    ], $this->businessDashboard->getStatsInfo($this->getUser()))
                            );
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<img src="./img/logo_.png" style="width: 50px;height: 40px;"/>')
            ->renderContentMaximized()
            ->renderSidebarMinimized()
            ;
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('css/admin.css')
            ->addJsFile('js/Chart.min.js')
            ->addJsFile('js/dashboard/chart-area-demo.js')
            ->addJsFile('js/dashboard/chart-pie-demo.js')
            ->addJsFile('js/dashboard/chart-horizontal.js')
            ->addJsFile('js/dashboard/chart-request.js')
            ->addCssFile('css/chart.css')
            ;
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),
            MenuItem::linkToCrud('Withdraw', 'fa fa-funnel-dollar', WithdrawalRequest::class)->setPermission("ROLE_PROMOTER"),

        ];
    }
}
