<?php

namespace App\Controller\Admin;

use App\Admin\Field\CustomImageField;
use App\Entity\Cover;
use App\Entity\Event;
use App\Entity\EventDate;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\InsufficientEntityPermissionException;
use EasyCorp\Bundle\EasyAdminBundle\Factory\EntityFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AvatarField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\PercentField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\Model\FileUploadState;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityUpdater;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;
use function PHPSTORM_META\type;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EventCrudController extends AbstractCrudController
{
    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb= $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->andWhere('entity.owner = :owner_id')->setParameter('owner_id', $this->getUser()->getId());
        return $qb;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $request = $this->get("request_stack")->getCurrentRequest();
        if(isset($request->files->get("Event")["coverFile"]["file"]))
        {
            $entityInstance->getCover()->setFile($request->files->get("Event")["coverFile"]["file"]);
            $entityInstance->getCover()->setUpdatedAt(new \DateTime());

        }
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $request = $this->get("request_stack")->getCurrentRequest();
        $coverFile = $request->files->get("Event")["coverFile"]["file"];
        $cover = new Cover($coverFile);
        $entityInstance->setCover($cover);
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function getEntityIdFromRequest()
    {
        $request = $this->get("request_stack")->getCurrentRequest();
        return $request->get("entityId");

    }


    public static function getEntityFqcn(): string
    {
        return Event::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Event')->setPageTitle("new", "add new event")//detail, edit, index, new
            ->setPageTitle('index', 'Events')
            ->setDefaultSort(['id' => 'DESC']);
            ;
    }


    public function configureFields(string $pageName): iterable
    {
        $categories = ['Sport' => 'Sport', 'Cinema' => 'Cinema', 'Theatre' => 'Theatre', 'Concerts' => 'Concerts', 'Forums et conferences' => 'Forums et conferences', 'Festival' => 'Festival'];
        if ($pageName === Crud::PAGE_INDEX) {
            return [
                IdField::new('id'),
                TextField::new('name'),
                TextField::new('category'),
                ImageField::new('coverUrl', 'Cover'),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        } elseif ($pageName === Crud::PAGE_DETAIL) {
            return [
                IdField::new('id'),
                TextField::new('name'),
                BooleanField::new('enabled'),
                TextField::new('category'),
                ImageField::new('cover')->setBasePath("")->setTemplatePath("cover.html.twig"),
                //CollectionField::new('eventDates', 'Dates')->setEntryType(EventDate::class),
                TextareaField::new('description'),
                AssociationField::new('created_by'),
                //PercentField::new('evtickPercentage'),
                TextField::new('getTotalPaidSales', 'Total sales')->formatValue(function ($value) {
                    return $value." CFA";
                }),
                Field::new('getTotalBenefit', 'Platform Benefits')->formatValue(function ($value) {
                    return $value." CFA";
                }),
                PercentField::new('evtickPercentage', 'Platform Benefits %'),
                Field::new('getTotalReceivedByOwner', 'Owner benefits')->formatValue(function ($value) {
                    return $value." CFA";
                }),
                //AssociationField::new('owner', 'owner')->autocomplete(),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        } elseif ($pageName === Crud::PAGE_NEW) {
            return [
                TextField::new('name'),
                ChoiceField::new('category')->setChoices($categories),
                TextareaField::new('description'),
                ImageField::new('coverFile')->setUploadDir('var')->setRequired(false),
                PercentField::new('evtickPercentage'),
                AssociationField::new('owner', 'owner')->autocomplete(),

            ];
        } elseif ($pageName === Crud::PAGE_EDIT) {
            return [
                TextField::new('name'),
                BooleanField::new('enabled'),
                ChoiceField::new('category')->setChoices($categories),
                ImageField::new('coverFile', 'Cover')->setUploadDir('var')->setRequired(false),
                TextareaField::new('description'),
                AssociationField::new('owner', 'owner')->autocomplete(),
            ];
        }
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('category')
            ->add('enabled')
            ->add('createdAt');
    }

    public function configureActions(Actions $actions): Actions
    {
        /**
         * @var $gene CrudUrlGenerator
         */
        $gene = $this->get(CrudUrlGenerator::class);
        $urlAddTicket = $gene->build()
            ->setController(TicketCrudController::class)
            ->setAction(Action::NEW)
            ->set('custom_event_id', $this->getEntityIdFromRequest())
            ->generateUrl();

        $urlListTicket = $gene->build()
            ->setController(TicketCrudController::class)
            ->setAction(Action::INDEX)
            ->set('custom_event_id', $this->getEntityIdFromRequest())
            ->generateUrl();
        $urlListDates = $gene->build()
            ->setController(EventDateCrudController::class)
            ->setAction(Action::INDEX)
            ->set('custom_event_id', $this->getEntityIdFromRequest())
            ->generateUrl();

        $addTicketAction = Action::new("add_ticket_for_event", "add ticket", "fa fa-ticket-alt")
            ->setCssClass("btn btn-info")
            ->linkToUrl($urlAddTicket);

        $listTicketAction = Action::new("list_ticket_for_event", "Tickets", "fa fa-ticket-alt")
            ->setCssClass("btn btn-info")
            ->linkToUrl($urlListTicket);
        $listDatesAction = Action::new("list_dates_for_event", "Dates", "fa fa-clock")
            ->setCssClass("btn btn-success")
            ->linkToUrl($urlListDates);

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::SAVE_AND_ADD_ANOTHER, Action::SAVE_AND_CONTINUE, Action::NEW, Action::EDIT)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ->add(Crud::PAGE_EDIT, $listDatesAction)
            ->add(Crud::PAGE_EDIT, $listTicketAction)
            ->add(Crud::PAGE_DETAIL, $listTicketAction)
            ->add(Crud::PAGE_DETAIL, $listDatesAction);
            //->add(Crud::PAGE_EDIT, Action::INDEX);
    }

    public function new(AdminContext $context)
    {
        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $context->getEntity()->setInstance($this->createEntity($context->getEntity()->getFqcn()));
        $this->get(EntityFactory::class)->processFields($context->getEntity(), FieldCollection::new($this->configureFields(Crud::PAGE_NEW)));
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $context->getCrud()->getActionsConfig());

        $newForm = $this->createNewForm($context->getEntity(), $context->getCrud()->getNewFormOptions(), $context);
        $newForm->handleRequest($context->getRequest());

        $entityInstance = $newForm->getData();
        $context->getEntity()->setInstance($entityInstance);

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            //  $this->processUploadedFiles($newForm);

            $event = new BeforeEntityPersistedEvent($entityInstance);
            $this->get('event_dispatcher')->dispatch($event);
            $entityInstance = $event->getEntityInstance();

            $this->persistEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $entityInstance);

            $this->get('event_dispatcher')->dispatch(new AfterEntityPersistedEvent($entityInstance));
            $context->getEntity()->setInstance($entityInstance);

            $submitButtonName = $context->getRequest()->request->get('ea')['newForm']['btn'];
            if (Action::SAVE_AND_CONTINUE === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_RETURN === $submitButtonName) {
                /*$url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();
                */
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setController(EventDateCrudController::class)
                    ->setAction(Action::NEW)
                    ->set('custom_event_id', $entityInstance->getId())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_ADD_ANOTHER === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()->setAction(Action::NEW)->generateUrl();

                return $this->redirect($url);
            }

            return $this->redirectToRoute($context->getDashboardRouteName());
        }

        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => Crud::PAGE_NEW,
            'templateName' => 'crud/new',
            'entity' => $context->getEntity(),
            'new_form' => $newForm,
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $responseParameters;
    }

    public function edit(AdminContext $context)
    {
        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $this->get(EntityFactory::class)->processFields($context->getEntity(), FieldCollection::new($this->configureFields(Crud::PAGE_EDIT)));
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $context->getCrud()->getActionsConfig());
        $entityInstance = $context->getEntity()->getInstance();

        if ($context->getRequest()->isXmlHttpRequest()) {
            $fieldName = $context->getRequest()->query->get('fieldName');
            $newValue = 'true' === mb_strtolower($context->getRequest()->query->get('newValue'));

            $event = $this->ajaxEdit($context->getEntity(), $fieldName, $newValue);
            if ($event->isPropagationStopped()) {
                return $event->getResponse();
            }

            // cast to integer instead of string to avoid sending empty responses for 'false'
            return new Response((int) $newValue);
        }

        $editForm = $this->createEditForm($context->getEntity(), $context->getCrud()->getEditFormOptions(), $context);
        $editForm->handleRequest($context->getRequest());
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //$this->processUploadedFiles($editForm);

            $event = new BeforeEntityUpdatedEvent($entityInstance);
            $this->get('event_dispatcher')->dispatch($event);
            $entityInstance = $event->getEntityInstance();

            $this->updateEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $entityInstance);

            $this->get('event_dispatcher')->dispatch(new AfterEntityUpdatedEvent($entityInstance));

            $submitButtonName = $context->getRequest()->request->get('ea')['newForm']['btn'];
            if (Action::SAVE_AND_CONTINUE === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_RETURN === $submitButtonName) {
                $url = empty($context->getReferrer())
                    ? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl()
                    : $context->getReferrer();

                return $this->redirect($url);
            }

            return $this->redirectToRoute($context->getDashboardRouteName());
        }

        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => Crud::PAGE_EDIT,
            'templateName' => 'crud/edit',
            'edit_form' => $editForm,
            'entity' => $context->getEntity(),
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $responseParameters;
    }

    private function ajaxEdit(EntityDto $entityDto, ?string $propertyName, bool $newValue): AfterCrudActionEvent
    {
        if (!$entityDto->hasProperty($propertyName)) {
            throw new \RuntimeException(sprintf('The "%s" boolean field cannot be changed because it doesn\'t exist in the "%s" entity.', $propertyName, $entityDto->getName()));
        }

        $this->get(EntityUpdater::class)->updateProperty($entityDto, $propertyName, $newValue);

        $event = new BeforeEntityUpdatedEvent($entityDto->getInstance());
        $this->get('event_dispatcher')->dispatch($event);
        $entityInstance = $event->getEntityInstance();

        $this->updateEntity($this->get('doctrine')->getManagerForClass($entityDto->getFqcn()), $entityInstance);

        $this->get('event_dispatcher')->dispatch(new AfterEntityUpdatedEvent($entityInstance));

        $entityDto->setInstance($entityInstance);

        $parameters = KeyValueStore::new([
            'action' => Action::EDIT,
            'entity' => $entityDto,
        ]);

        $event = new AfterCrudActionEvent($this->getContext(), $parameters);
        $this->get('event_dispatcher')->dispatch($event);

        return $event;
    }
    private function getContext(): ?AdminContext
    {
        return $this->get(AdminContextProvider::class)->getContext();
    }

    public function createEntity(string $entityFqcn)
    {
        $event = new Event();
        $event->setEnabled(false);
        $event->setCreatedBy($this->getUser());
        return $event;
    }
}
