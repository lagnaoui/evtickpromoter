<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\EventDate;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\InsufficientEntityPermissionException;
use EasyCorp\Bundle\EasyAdminBundle\Factory\EntityFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;

class EventDateCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return $crud/*->setDateTimeFormat("Y-m-d h:i:s")*/
        ->setPageTitle('index', 'Event\'s dates')
            ->setDefaultSort(['id' => 'DESC']);
    }
    public static function getEntityFqcn(): string
    {
        return EventDate::class;
    }

    public function getEventIdFromRequest()
    {
        $request = $this->get("request_stack")->getCurrentRequest();
        return $request->get("custom_event_id");

    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb= $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->andWhere('entity.event = :event_id')->setParameter('event_id', $this->getEventIdFromRequest());
        return $qb;
    }

    public function configureFields(string $pageName): iterable
    {
        $eventId = $this->getEventIdFromRequest();
        if ($pageName === Crud::PAGE_INDEX) {
            return [
                Field::new('id'),
                AssociationField::new('event', 'Event'),
                DateTimeField::new('start_date'),
                DateTimeField::new('end_date'),
                AssociationField::new('position', 'city'),

            ];
        } elseif ($pageName === Crud::PAGE_DETAIL) {
            return [
                Field::new('id'),
                AssociationField::new('event', 'Event'),
                DateTimeField::new('start_date'),
                DateTimeField::new('end_date'),
                AssociationField::new('position', 'city'),
                Field::new('latitude'),
                Field::new('longitude'),
                TextareaField::new('address'),

            ];
        } elseif ($pageName === Crud::PAGE_NEW) {
            $event = AssociationField::new('event', 'Event')->setRequired(true);
            if($eventId)
                $event->setFormTypeOption('disabled','disabled');

            return [
                $event,
                DateTimeField::new('start_date'),
                DateTimeField::new('end_date'),
                AssociationField::new('position', 'city')->setRequired(true),
                Field::new('latitude'),
                Field::new('longitude'),
                TextareaField::new('address'),
            ];
        }elseif ($pageName === Crud::PAGE_EDIT)
        {
            $event = AssociationField::new('event', 'Event')->setRequired(true);
            if($eventId)
                $event->setFormTypeOption('disabled','disabled');

            return [
                $event,
                DateTimeField::new('start_date'),
                DateTimeField::new('end_date'),
                AssociationField::new('position', 'city')->setRequired(true),
                Field::new('latitude'),
                Field::new('longitude'),
                TextareaField::new('address'),
            ];


        }
    }

    public function new(AdminContext $context)
    {
        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $context->getEntity()->setInstance($this->createEntity($context->getEntity()->getFqcn()));
        $this->get(EntityFactory::class)->processFields($context->getEntity(), FieldCollection::new($this->configureFields(Crud::PAGE_NEW)));
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $context->getCrud()->getActionsConfig());

        $newForm = $this->createNewForm($context->getEntity(), $context->getCrud()->getNewFormOptions(), $context);
        $newForm->handleRequest($context->getRequest());

        $entityInstance = $newForm->getData();
        $context->getEntity()->setInstance($entityInstance);

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            $this->processUploadedFiles($newForm);

            $event = new BeforeEntityPersistedEvent($entityInstance);
            $this->get('event_dispatcher')->dispatch($event);
            $entityInstance = $event->getEntityInstance();

            $this->persistEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $entityInstance);

            $this->get('event_dispatcher')->dispatch(new AfterEntityPersistedEvent($entityInstance));
            $context->getEntity()->setInstance($entityInstance);

            $submitButtonName = $context->getRequest()->request->get('ea')['newForm']['btn'];
            if (Action::SAVE_AND_CONTINUE === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_RETURN === $submitButtonName) {
                /*$url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();
                */
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setController(EventCrudController::class)
                    ->setAction(Action::DETAIL)
                    ->setEntityId($this->getEventIdFromRequest())
                    ->set('custom_event_id', $this->getEventIdFromRequest())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_ADD_ANOTHER === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()->setAction(Action::NEW)->generateUrl();

                return $this->redirect($url);
            }

            return $this->redirectToRoute($context->getDashboardRouteName());
        }

        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => Crud::PAGE_NEW,
            'templateName' => 'crud/new',
            'entity' => $context->getEntity(),
            'new_form' => $newForm,
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $responseParameters;
    }


    public function createEntity(string $entityFqcn)
    {
        $eventDate = new EventDate();
        $request = $this->get("request_stack")->getCurrentRequest();
        if($request->query->get("custom_event_id"))
        {
            $event = $this->getDoctrine()->getRepository(Event::class)->find($request->query->get("custom_event_id"));
            $eventDate->setEvent($event);
        }
        return $eventDate;
    }

    public function configureActions(Actions $actions): Actions
    {

        $actionsToRender = $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::EDIT, Action::NEW)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ;

        if(empty($this->getEventIdFromRequest()))
        {
            $actionsToRender->disable(Action::DELETE, Action::EDIT, Action::NEW, Action::INDEX);
        }

        return $actionsToRender;
    }

}
