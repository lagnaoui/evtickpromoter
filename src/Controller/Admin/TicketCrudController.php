<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\Ticket;
use App\Repository\EventDateRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ColorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class TicketCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return $crud/*->setDateTimeFormat("Y-m-d h:i:s")*/
        ->setPageTitle('index', 'Tickets')
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function getEventIdFromRequest()
    {
        $request = $this->get("request_stack")->getCurrentRequest();
        return $request->get("custom_event_id");

    }
    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb= $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->leftJoin(EventDate::class, 'ev','WITH', 'ev.id = entity.eventDate' )
            ->leftJoin(Event::class, 'evt', 'WITH', 'evt.id = ev.event')
            ->andWhere('ev.event = :event_id')->setParameter('event_id', $this->getEventIdFromRequest());
        return $qb;
    }


    public static function getEntityFqcn(): string
    {
        return Ticket::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $eventDates = AssociationField::new('eventDate')->setRequired(true);
        if($this->getEventIdFromRequest())
        {
            $eventDates->setFormTypeOptions([
                'query_builder' => function (EventDateRepository $ed) {
                    return $ed->createQueryBuilder('u')
                        ->where('u.event = :event_id')
                        ->setParameter('event_id', $this->getEventIdFromRequest());
                        //->orderBy('u.las', 'ASC');
                },
            ]);

        }

        if ($pageName === Crud::PAGE_INDEX) {
            return [
                $eventDates,
                ChoiceField::new('type')->setChoices([
                    "GOLD" => "GOLD",
                    "LUXURY" => "LUXURY",
                    "STARS" => "STARS",
                    "Bronze" => "Bronze",
                    "Argent" => "Argent",
                    "Or" => "Or",
                    "Diamant" => "Diamant",
                ]),
                ColorField::new('color'),
                IntegerField::new('quantity'),
                Field::new('price'),
                ChoiceField::new('currency_code')->setChoices(["CFA"=>"CFA"])

            ];

        } elseif ($pageName === Crud::PAGE_DETAIL) {
            return [
                $eventDates,
                ChoiceField::new('type')->setChoices([
                    "GOLD" => "GOLD",
                    "LUXURY" => "LUXURY",
                    "STARS" => "STARS",
                    "Bronze" => "Bronze",
                    "Argent" => "Argent",
                    "Or" => "Or",
                    "Diamant" => "Diamant",
                ]),
                ColorField::new('color'),
                IntegerField::new('quantity'),
                Field::new('price'),
                ChoiceField::new('currency_code')->setChoices(["CFA"=>"CFA"])

            ];

        } elseif ($pageName === Crud::PAGE_NEW) {
            return [
                $eventDates,
                ChoiceField::new('type')->setChoices([
                    "GOLD" => "GOLD",
                    "LUXURY" => "LUXURY",
                    "STARS" => "STARS",
                    "Bronze" => "Bronze",
                    "Argent" => "Argent",
                    "Or" => "Or",
                    "Diamant" => "Diamant",
                ]),
                ColorField::new('color'),
                IntegerField::new('quantity'),
                Field::new('price'),
                ChoiceField::new('currency_code')->setChoices(["CFA"=>"CFA"])

            ];
        }elseif ($pageName === Crud::PAGE_EDIT)
        {
            return [
                $eventDates->setFormTypeOption('disabled','disabled'),
                ChoiceField::new('type')->setChoices([
                    "GOLD" => "GOLD",
                    "LUXURY" => "LUXURY",
                    "STARS" => "STARS",
                    "Bronze" => "Bronze",
                    "Argent" => "Argent",
                    "Or" => "Or",
                    "Diamant" => "Diamant",
                ])->setFormTypeOption('disabled','disabled'),
                ColorField::new('color'),
                IntegerField::new('quantity'),
                Field::new('price'),
                ChoiceField::new('currency_code')->setChoices(["CFA"=>"CFA"])

            ];
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setInitQuantity($entityInstance->getQuantity());
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::NEW, Action::EDIT)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ;
    }

}
