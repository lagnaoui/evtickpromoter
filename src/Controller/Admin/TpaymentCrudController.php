<?php

namespace App\Controller\Admin;

use App\Entity\Tpayment;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use App\Entity\Torder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;



class TpaymentCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return $crud/*->setDateTimeFormat("Y-m-d h:i:s")*/
        ->setPageTitle('index', 'Payments')
            ->setDefaultSort(['id' => 'DESC']);
    }
    public static function getEntityFqcn(): string
    {
        return Tpayment::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if ($pageName === Crud::PAGE_INDEX) {
            return [
                IdField::new('id'),
                TextField::new('tid', 'transaction id'),
                AssociationField::new('torder', 'order'),
                TextField::new('state'),
                TextField::new('payment_method', 'payment method'),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        } elseif ($pageName === Crud::PAGE_DETAIL) {
            return [
                IdField::new('id'),
                TextField::new('tid', 'transaction id'),
                TextField::new('state'),
                TextareaField::new('purchase'),
                TextField::new('payment_method', 'payment method'),
                AssociationField::new('torder', 'order'),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        }
    }
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('tid')
            ->add('state')
            ->add('payment_method')
            ->add('createdAt');
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::SAVE_AND_ADD_ANOTHER, Action::SAVE_AND_CONTINUE, Crud::PAGE_EDIT, Crud::PAGE_NEW, Crud::PAGE_INDEX)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ;
    }
}
