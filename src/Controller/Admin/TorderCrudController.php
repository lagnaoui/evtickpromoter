<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\OrderItem;
use App\Entity\Ticket;
use App\Entity\Torder;
use App\Filter\EventFilter;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityUpdater;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;

class TorderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Torder::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setPageTitle(Action::INDEX, "Orders")
            ->setDefaultSort(['id' => 'DESC']);;
    ;
}

    public function configureAssets(Assets $assets): Assets
    {
        return Assets::new()->addCssFile('css/order.css');
    }
    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = entity.id')
        ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
        ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
        ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
        ->andWhere('e.owner = :owner')
        ->setParameter('owner', $this->getUser()->getId());
        return $qb;
    }


    public function configureFields(string $pageName): iterable
    {
        if ($pageName === Crud::PAGE_INDEX) {
            return [
                IdField::new('id'),
                TextField::new('number'),
                TextField::new('getFirstEvent', 'Event'),
                TextField::new('state')->setTemplatePath("state.html.twig"),
                AssociationField::new('user', 'Customer'),
                NumberField::new('items_total', 'total'),
                TextField::new('currency_code'),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        } elseif ($pageName === Crud::PAGE_DETAIL) {
            return [
                IdField::new('id'),
                TextField::new('number'),
                AssociationField::new('tpayment', 'payment'),
                TextField::new('state')->setTemplatePath("state.html.twig"),
                AssociationField::new('user', 'Customer'),
                AssociationField::new('beneficiary'),
                NumberField::new('items_total'),
                AssociationField::new('orderItems')->setTemplatePath("items.html.twig"),
                TextField::new('customer_ip'),
                DateTimeField::new('createdAt', 'Created at'),
            ];
        }
    }
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('number')
            ->add('state')
            ->add('user')
            ->add('items_total')
            ->add(EventFilter::new('event'))
            ->add('createdAt');
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::SAVE_AND_ADD_ANOTHER, Action::SAVE_AND_CONTINUE, Crud::PAGE_EDIT, Crud::PAGE_NEW)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ;
    }
}
