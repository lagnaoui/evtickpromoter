<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AdminCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Admin::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if ($pageName === Crud::PAGE_INDEX)
        {
            return [
                IdField::new('id'),
                TextField::new('username'),
                TextField::new('fullName'),
                BooleanField::new('enabled'),
                ChoiceField::new('roles', 'Roles')
                    ->allowMultipleChoices()
                    ->autocomplete()
                    ->setChoices([
                            'ADMIN' => 'ROLE_ADMIN',
                            'SUPERADMIN' => 'ROLE_SUPER_ADMIN',
                            'PROMOTER'=>'ROLE_PROMOTER']
                    ),
            ];

        } elseif ($pageName === Crud::PAGE_DETAIL)
        {
            return [
                //IdField::new('id'),
                TextField::new('username'),
                TextField::new('fullName'),
                ChoiceField::new('roles', 'Roles')
                    ->allowMultipleChoices()
                    ->autocomplete()
                    ->setChoices([
                            'ADMIN' => 'ROLE_ADMIN',
                            'SUPERADMIN' => 'ROLE_SUPER_ADMIN',
                            'PROMOTER'=>'ROLE_PROMOTER']
                    ),
                EmailField::new('email'),
                TelephoneField::new('phone'),
                AssociationField::new('position', 'city'),
                TextareaField::new('address'),
                //BooleanField::new('enabled'),
            ];

        } elseif ($pageName === Crud::PAGE_NEW)
        {
            return [
                TextField::new('username'),
                TextField::new('fullName'),
                TextField::new('entityName')->setRequired(true),
                ChoiceField::new('roles', 'Roles')
                    ->allowMultipleChoices()
                    ->autocomplete()
                    ->setChoices([
                            'ADMIN' => 'ROLE_ADMIN',
                            'SUPERADMIN' => 'ROLE_SUPER_ADMIN',
                            'PROMOTER'=>'ROLE_PROMOTER']
                    ),
                EmailField::new('email'),
                TelephoneField::new('phone'),
                AssociationField::new('position', 'city'),
                TextareaField::new('address'),
                TextField::new('password')->setFormType(RepeatedType::class)->setFormTypeOptions([
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'New password:'],
                    'second_options' => ['label' => 'Repeat password:'],
                ]),
            ];

        } elseif ($pageName === Crud::PAGE_EDIT)
        {
            return [
                TextField::new('username'),
                TextField::new('fullName'),
                TextField::new('entityName'),
                BooleanField::new('enabled'),
                ChoiceField::new('roles', 'Roles')
                    ->allowMultipleChoices()
                    ->autocomplete()
                    ->setChoices([  'User' => 'ROLE_USER',
                            'Admin' => 'ROLE_ADMIN',
                            'SuperAdmin' => 'ROLE_SUPER_ADMIN']
                    ),
                EmailField::new('email'),
                TelephoneField::new('phone'),
                AssociationField::new('position', 'city'),
                TextareaField::new('address'),
                TextField::new('password')->setFormType(RepeatedType::class)->setFormTypeOptions([
                    'type' => PasswordType::class,
                    'first_options' => ['label' => 'New password:'],
                    'second_options' => ['label' => 'Repeat password:'],
                ])->setRequired(false),
            ];

        }
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('username')
            ->add('fullName')
            ->add('entityName')
            ->add('position')
            ->add('createdAt');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::SAVE_AND_ADD_ANOTHER, Action::SAVE_AND_CONTINUE, Action::NEW, Action::EDIT, Action::INDEX)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ->add(Crud::PAGE_EDIT, Action::INDEX);
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setPassword(password_hash($entityInstance->getPassword(), PASSWORD_BCRYPT));
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setPassword(password_hash($entityInstance->getPassword(), PASSWORD_BCRYPT));
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

}
