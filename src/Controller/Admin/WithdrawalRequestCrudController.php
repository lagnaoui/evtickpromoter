<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\WithdrawalRequest;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Exception\ForbiddenActionException;
use EasyCorp\Bundle\EasyAdminBundle\Exception\InsufficientEntityPermissionException;
use EasyCorp\Bundle\EasyAdminBundle\Factory\EntityFactory;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\PercentField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Security\Permission;
use Symfony\Component\HttpFoundation\RedirectResponse;

class WithdrawalRequestCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WithdrawalRequest::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud/*->setDateTimeFormat("Y-m-d h:i:s")*/
            ->setPageTitle('index', 'Withdrawals');
    }
    public function configureAssets(Assets $assets): Assets
    {
        return Assets::new()->addCssFile('css/order.css');
    }

    public function configureFields(string $pageName): iterable
    {
        if($pageName === Crud::PAGE_INDEX)
        {
            return [
                AssociationField::new('eventDate', 'Event')->setQueryBuilder(function ($qb){
                    return $qb
                        ->leftJoin(WithdrawalRequest::class, 'w', 'WITH', 'w.eventDate = entity')
                        ->leftJoin(Event::class, 'e', 'WITH', 'e.id = entity.event')
                        ->andWhere('w IS NULL')
                        ->andWhere('e.owner = :owner')
                        ->andWhere("entity.end_date < :today")
                        ->setParameter('today', new \DateTime())
                        ->setParameter('owner', $this->getUser()->getId());
                }),
                //TextareaField::new('rib', 'Iban'),
                TextField::new('fullname', 'Fullname'),
                TextField::new('state', 'State')->setTemplatePath("state_withdraw.html.twig"),
                DateTimeField::new('created_at'),

            ];


        }elseif ($pageName === Crud::PAGE_DETAIL)
        {
            return [
                AssociationField::new('eventDate', 'Event')->setQueryBuilder(function ($qb){
                    return $qb
                        ->leftJoin(WithdrawalRequest::class, 'w', 'WITH', 'w.eventDate = entity')
                        ->leftJoin(Event::class, 'e', 'WITH', 'e.id = entity.event')
                        ->andWhere('w IS NULL')
                        ->andWhere('e.owner = :owner')
                        ->andWhere("entity.end_date < :today")
                        ->setParameter('today', new \DateTime())
                        ->setParameter('owner', $this->getUser()->getId());
                }),
                TextareaField::new('rib', 'Iban'),
                TextField::new('fullname', 'Fullname'),
                AssociationField::new('verifiedBy', 'Verified by'),
                TextField::new('state', 'State')->setTemplatePath("state_withdraw.html.twig"),
                DateTimeField::new('created_at'),
                TextField::new('eventDate.getTotalPaidSales', 'Total sales')->formatValue(function ($value) {
                    return $value." CFA";
                }),
                Field::new('eventDate.getTotalBenefit', 'Platform Benefits')->formatValue(function ($value) {
                    return $value." CFA";
                }),
                PercentField::new('eventDate.event.evtickPercentage', 'Platform Benefits %'),
                Field::new('eventDate.getTotalReceivedByOwner', 'Owner benefits')->formatValue(function ($value) {
                    return $value." CFA";
                }),

            ];

        }else
            {
                return [
                    AssociationField::new('eventDate', 'Event')->setQueryBuilder(function ($qb){
                        return $qb
                            ->leftJoin(WithdrawalRequest::class, 'w', 'WITH', 'w.eventDate = entity')
                            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = entity.event')
                            ->andWhere('w IS NULL')
                            ->andWhere('e.owner = :owner')
                            ->andWhere("entity.end_date < :today")
                            ->setParameter('today', new \DateTime())
                            ->setParameter('owner', $this->getUser()->getId());
                    }),
                    TextareaField::new('rib', 'Iban'),
                    TextField::new('fullname', 'Fullname'),
                ];

            }

    }
    public function configureActions(Actions $actions): Actions
    {
        $actionsToRender = $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::EDIT)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action->setLabel("withdraw")->setIcon('fa-fw fa fa-funnel-dollar');
            })
        ;
        return $actionsToRender;
    }
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('state')
            ->add('rib')
            ->add('Fullname')
            ->add('createdAt');
    }
    public function createEntity(string $entityFqcn)
    {
        $withdrawal = new WithdrawalRequest();
        $withdrawal->setOwner($this->getUser());
        $withdrawal->setState('requested');
        return $withdrawal;
    }

    public function new(AdminContext $context)
    {
        $event = new BeforeCrudActionEvent($context);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        if (!$this->isGranted(Permission::EA_EXECUTE_ACTION)) {
            throw new ForbiddenActionException($context);
        }

        if (!$context->getEntity()->isAccessible()) {
            throw new InsufficientEntityPermissionException($context);
        }

        $context->getEntity()->setInstance($this->createEntity($context->getEntity()->getFqcn()));
        $this->get(EntityFactory::class)->processFields($context->getEntity(), FieldCollection::new($this->configureFields(Crud::PAGE_NEW)));
        $this->get(EntityFactory::class)->processActions($context->getEntity(), $context->getCrud()->getActionsConfig());

        $newForm = $this->createNewForm($context->getEntity(), $context->getCrud()->getNewFormOptions(), $context);
        $newForm->handleRequest($context->getRequest());

        $entityInstance = $newForm->getData();
        $context->getEntity()->setInstance($entityInstance);

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            $this->processUploadedFiles($newForm);

            $event = new BeforeEntityPersistedEvent($entityInstance);
            $this->get('event_dispatcher')->dispatch($event);
            $entityInstance = $event->getEntityInstance();

            $this->persistEntity($this->get('doctrine')->getManagerForClass($context->getEntity()->getFqcn()), $entityInstance);

            $this->get('event_dispatcher')->dispatch(new AfterEntityPersistedEvent($entityInstance));
            $context->getEntity()->setInstance($entityInstance);

            $submitButtonName = $context->getRequest()->request->get('ea')['newForm']['btn'];
            if (Action::SAVE_AND_CONTINUE === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_RETURN === $submitButtonName) {
                /*$url = $context->getReferrer()
                    ?? $this->get(CrudUrlGenerator::class)->build()->setAction(Action::INDEX)->generateUrl();

                return $this->redirect($url);*/
                $url = $this->get(CrudUrlGenerator::class)
                    ->build()
                    ->setAction(Action::DETAIL)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl();

                return $this->redirect($url);
            }

            if (Action::SAVE_AND_ADD_ANOTHER === $submitButtonName) {
                $url = $this->get(CrudUrlGenerator::class)->build()->setAction(Action::NEW)->generateUrl();

                return $this->redirect($url);
            }

            return $this->redirectToRoute($context->getDashboardRouteName());
        }

        $responseParameters = $this->configureResponseParameters(KeyValueStore::new([
            'pageName' => Crud::PAGE_NEW,
            'templateName' => 'crud/new',
            'entity' => $context->getEntity(),
            'new_form' => $newForm,
        ]));

        $event = new AfterCrudActionEvent($context, $responseParameters);
        $this->get('event_dispatcher')->dispatch($event);
        if ($event->isPropagationStopped()) {
            return $event->getResponse();
        }

        return $responseParameters;
    }


}
