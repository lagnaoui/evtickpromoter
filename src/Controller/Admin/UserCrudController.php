<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Entity\EventDate;
use App\Entity\OrderItem;
use App\Entity\Ticket;
use App\Entity\Torder;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud/*->setDateTimeFormat("Y-m-d h:i:s")*/
        ->setPageTitle('index', 'Customers')
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $qb->leftJoin(Torder::class, 'o', 'WITH', 'o.user = entity.id')
            ->leftJoin(OrderItem::class, 'ot', 'WITH', 'ot.torder = o.id')
            ->leftJoin(Ticket::class, 't', 'WITH', 't.id = ot.ticket')
            ->leftJoin(EventDate::class, 'ed', 'WITH', 'ed.id = t.eventDate')
            ->leftJoin(Event::class, 'e', 'WITH', 'e.id = ed.event')
            ->andWhere('e.owner = :owner')
            ->setParameter('owner', $this->getUser()->getId());
        return $qb;
    }


    public function configureFields(string $pageName): iterable
    {
        if ($pageName === Crud::PAGE_INDEX)
        {
            return [
                IdField::new('id'),
                ImageField::new('avatarUrl', 'Avatar'),
                TextField::new('fullName'),
                TextField::new('City'),
                TelephoneField::new('phone'),
                EmailField::new('email'),
            ];


        } elseif ($pageName === Crud::PAGE_DETAIL)
        {
            return [
                IdField::new('id'),
                ImageField::new('avatar')->setBasePath("")->setTemplatePath("cover.html.twig"),
                TextField::new('fullName'),
                TelephoneField::new('phone'),
                EmailField::new('email'),
                TextField::new('City'),
                TextField::new('Country'),
                TextField::new('latitude'),
                TextField::new('longitude'),
                DateTimeField::new('created_at'),
                TextField::new('device.os', 'Platform'),
                TextField::new('device.model', 'Model'),
                TextField::new('device.version', 'Installed version'),
            ];


        }
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('id')
            ->add('fullName')
            ->add('phone')
            ->add('position')
            ->add('email')
            ->add('createdAt');
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->disable(Action::DELETE, Action::SAVE_AND_ADD_ANOTHER, Action::SAVE_AND_CONTINUE, Action::EDIT, Action::NEW)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel(false);
            })
            ;
    }

}
