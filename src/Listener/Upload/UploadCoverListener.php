<?php
/**
 * Created by PhpStorm.
 * User: MacBook
 * Date: 9/21/21
 * Time: 17:06
 */

namespace App\Listener\Upload;


use App\Component\FileUploader as FileUploader;
//use App\Component\LocalUploader as FileUploader;
use App\Entity\Cover;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UploadCoverListener
{
    public const FOLDER = "cover";
    private $uploader;
    private $params;
    public function __construct(FileUploader $imageUpload, ParameterBagInterface $params)
    {
        $this->uploader = $imageUpload;
        $this->params = $params;
    }
    public function uploadCover(Cover $cover, LifecycleEventArgs $event)
    {
        ///$this->uploader->upload($cover, $this->params->get('kernel.project_dir').self::FOLDER);
        $this->uploader->upload($cover, self::FOLDER);
    }

}
