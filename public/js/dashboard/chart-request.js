
document.addEventListener('DOMContentLoaded', function () {



    var rawData = document.querySelector('.data-request-chart');
    var requests = rawData.dataset.requests.replace("[", "").replace("]", "").split(',');;

// Pie Chart Example
    var ctx = document.getElementById("requestChart");
    var data = requests;
    var label = ["Accepted", "Requested", "Refused"];
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: label,
            datasets: [{
                label: "Requests",
                backgroundColor: ["#3e95cd", "#3cba9f", "#e01a26"],
                data: data
            }]
        },
        options:{
            maintainAspectRatio: false,
            legend: {
                display: false
            },
        }
    });

});


