// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';


document.addEventListener('DOMContentLoaded', function () {
    var data = document.querySelector('.data-horiz-chart');
    let customer_number =  data.dataset.demographicsClient.replace("[", "").replace("]", "").split(',');//[50, 12, 8, 6, 19, 33, 38, 3, 1, 2, 18];
    let chef_number = data.dataset.demographicsChef.replace("[", "").replace("]", "").split(',');//[5, 10, 81, 98, 11, 32, 30, 23, 2, 0, 0];
    let cities = data.dataset.cities.replace("[", "").replace("]", "").split(',');//["Riyadh", "Jeddah", "Makkah", "Medina", "Al hofuf", "Taif", "Tabuk", "Jazan", "Najran", "Abha", "Eastern region"];

// Pie Chart Example
    var ctx = document.getElementById("myHorizontalChart");
    var data = [{
        label: 'Chef',
        backgroundColor: '#4e73df',
        data: chef_number
    }, {
        label: 'Customer',
        backgroundColor: '#1cc88a',
        data: customer_number
    }
    ];

    let myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: cities,
            datasets: data
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });





});
